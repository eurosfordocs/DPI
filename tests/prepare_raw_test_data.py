import pandas as pd
import os
import hashlib

import src.constants.files as files
import src.constants.columns as c
from src.utils import normalize_string

import logging


REAL_RAW_DATA_PATH = os.path.join(files.ROOT_PATH, "data", "raw")
TEST_RAW_DATA_PATH = os.path.join(files.ROOT_PATH, "data_TEST", "raw")

COLS_TO_HASH = [c.PRENOM_DECLARANT, c.NOM_DECLARANT, c.IDENTIFIANT_PROFESSIONNEL, c.TS_IDENTIFIANT,
                "Nom du déclarant", "Prénom du déclarant", "nom", "prénom"]

NROWS = 10000


def prepare_raw_test_data(force_recompute=False):
    real_raw_csv_files = [file for file in os.listdir(REAL_RAW_DATA_PATH) if ".csv" in file]
    test_raw_csv_files = [file for file in os.listdir(TEST_RAW_DATA_PATH) if ".csv" in file]

    files_to_copy_in_test = [file for file in real_raw_csv_files if file not in test_raw_csv_files]

    if force_recompute:
        files_to_copy_in_test = real_raw_csv_files

    for file in files_to_copy_in_test:
        logging.info(f"Truncating {file} and writing truncated version into test raw data folder.")

        file_path = os.path.join(REAL_RAW_DATA_PATH, file)
        if file.endswith(".csv"):
            df = pd.read_csv(file_path, sep=get_sep(file_path), nrows=NROWS)
            df = hash_personal_data(df)
            df.to_csv(os.path.join(TEST_RAW_DATA_PATH, file), sep=get_sep(file_path), index=False)
        if file.endswith(".csv.gz"):
            df = pd.read_csv(file_path, sep=";", nrows=NROWS, compression="gzip")
            df = hash_personal_data(df)
            df.to_csv(os.path.join(TEST_RAW_DATA_PATH, file), sep=";", compression="gzip", index=False)


def get_sep(file_path):
    """
    get separator of table file by reading first line.

    :param file_path: file path.
    :return: separator (; or ,)
    """
    with open(file_path, "r", encoding="utf8") as file:
        first_line = file.readline()

    for sep in [",", ";"]:
        if sep in first_line:
            return sep


def hash_personal_data(df):
    df_cols_to_hash = [col for col in df if col in COLS_TO_HASH]

    for col in df_cols_to_hash:
        df[col] = df[col].apply(
            lambda x: normalize_string(hashlib.sha256(normalize_string(str(x)).encode("utf-8")).hexdigest()[:10]))

    return df