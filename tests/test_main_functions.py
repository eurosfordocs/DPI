from src.dpi_ts_matching import get_dpi_not_in_ts
from src.preprocess.annuaire_pro import preprocess_annuaire_pro
from src.preprocess.lien_organisme_groupe_TS import preprocess_lien_organisme_groupe_ts
from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.preprocess.create_interim_ts import create_interim_ts
from src.utils import download_all_files
from src.kpi.kpi import update_kpi
from tests.prepare_raw_test_data import prepare_raw_test_data

import logging


def test():
    logging.info("Launching test.")
    prepare_raw_test_data(force_recompute=True)

    # download_all_files()

    preprocess_annuaire_pro()
    preprocess_lien_organisme_groupe_ts()
    create_interim_dpi()
    create_interim_ts(chunksize=10 ** 6)
    get_dpi_not_in_ts()

    update_kpi()
