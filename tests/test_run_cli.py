from click.testing import CliRunner

from main import cli

runner = CliRunner()


def test_run_cli_default():
    runner.invoke(cli, catch_exceptions=False)


def test_run_cli_no_download():
    runner.invoke(cli, '-nd', catch_exceptions=False)
