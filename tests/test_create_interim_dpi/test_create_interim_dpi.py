import os
import pandas as pd
import logging

from src.preprocess.create_interim_dpi.create_interim_dpi import preprocess_dpi
import src.constants.columns as c


LOCAT_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)))


def test_preprocess_dpi():
    input = pd.read_csv(os.path.join(LOCAT_ROOT, "input - raw dpi anonyme.csv"), sep=";")

    result = preprocess_dpi(input)

    logging.info("writing results csv")
    result.to_csv(os.path.join(LOCAT_ROOT, "result.csv"), index=False)

    expected = pd.read_csv(os.path.join(LOCAT_ROOT, "expected - preprocessed_dpi.csv"),
                           parse_dates=["date_debut", "date_fin"])
    # Read result from csv to avoid problems with nan
    result = pd.read_csv(os.path.join(LOCAT_ROOT, "result.csv"), parse_dates=["date_debut", "date_fin"])

    pd.testing.assert_frame_equal(result, expected, check_dtype=False)
