# DPI

Étudier les déclarations publiques d'intérêts des acteurs de la décision et de l'expertise sanitaires.

Les données proviennent du site de [consultation des déclarations publiques d'intérets](https://dpi.sante.gouv.fr).

## Installation

La commande `make install` installe le projet. 
Pour une procédure manuelle détaillée, voir le fichier [INSTALL.md](INSTALL.md).


## Usage

Organisation des dossiers :
- [notebooks](notebooks) contient les travaux exploratoires 
- [src](src) contient le code de préparation commun
- [test](tests) contient des tests automatisés

Le script `./main.py` à la racinne permet d'effectuer les travaux de préparation communs.

`./main.py --help` affiche l'aide 

## Note

La structure du projet est expliquée dans le projet [cookiecutter-data-fr](https://gitlab.com/pajachiet/cookiecutter-data-fr#la-structure-de-dossier-cr%C3%A9%C3%A9e-est-la-suivante).
