# Données

Ce dossier contient les données du projet. 

Nous ne versionnons pas les données, car elles contiennent des informations personnelles qui ne doivent pas être indexées.