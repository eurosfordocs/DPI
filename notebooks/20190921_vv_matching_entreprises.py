# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
# %matplotlib inline
pd.set_option("max_columns", 100)
pd.set_option('max_colwidth', 400)

# %%
data_path = "../data/interim/"
raw_data = "../data/raw/"
etalab_path = "../../exports-etalab/"

# %%
dpi = pd.read_csv(data_path + "dpi_no_name.csv", sep=";")
dpi.head()

# %%
ts_rem = pd.read_csv(etalab_path + "declaration_remuneration_2019_09_11_04_00.csv", sep=";")
ts_rem.head()

# %%
dpi_orgs = dpi[["organisme"]].drop_duplicates()
ts_orgs = ts_rem[["denomination_sociale"]].drop_duplicates()

print("Nombre d’organismes dpi: {}".format(len(dpi_orgs)))
print("Nombre d’organismes ts: {}".format(len(ts_orgs)))

# %%
'''
# Test avec un join basique
'''

# %%
simple_join = pd.merge(dpi_orgs, ts_orgs,
                       left_on="organisme", right_on="denomination_sociale",
                      how="inner")

# %%
print("Le matching par une jointure simple englobe {}% des dpi ({}).".format(
round(100*len(dpi[dpi["organisme"].isin(simple_join["organisme"])])/len(dpi), 1),
len(simple_join)))

# %%
'''
# Test avec l’inclusion des mots dpi dans ts
'''

# %%
import nltk
nltk.download("stopwords")

def org_to_word_list(org):
    word_list = [word.lower() for word in str(org).split(" ") if word not in nltk.corpus.stopwords.words('french')
                and "-" not in word]
    return word_list

ts_orgs["word_list"] = ts_orgs["denomination_sociale"].apply(org_to_word_list)
dpi_orgs["word_list"] = dpi_orgs["organisme"].apply(org_to_word_list)
dpi_orgs.head(3)

# %%
# Il faut que la liste de mots de l’organisme dpi et celle de ts aient au moins nb_required_matches mots en communs
# pour faire l’association

nb_required_matches = 2

matching_dict_list = []
for word_list in dpi_orgs["word_list"]:
    matching_ts_word_lists = []
    for word in word_list:
        for ts_word_list in ts_orgs["word_list"]:
            if len(set(word_list).intersection(set(ts_word_list))) >= nb_required_matches:
                matching_ts_word_lists.append(" ".join(ts_word_list))
    if len(matching_ts_word_lists) > 0:
        matching_dict_list.append({"organisme": " ".join(word_list), "matching_den_soc": set(matching_ts_word_lists)})

# %%
matching_df = pd.DataFrame.from_records(matching_dict_list)

# %%
matching_df.head(100)

# %%
print("Le matching par une jointure par match de {} mots englobe {}% des organismes ({}).".format(
    nb_required_matches,
    round(100*len(dpi_orgs[dpi_orgs["word_list"].apply(lambda x: " ".join(x)).isin(
        matching_df["organisme"])])/len(dpi_orgs), 1),
len(matching_df)))

# %%



