# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
# %matplotlib inline
pd.set_option("max_columns", 100)
pd.set_option('max_colwidth', 400)

interim_data = "../data/interim/"

# # Chargement DPI preprocess

dpi = pd.read_csv(interim_data + "20190903001500_export_declarations_publiees.csv", sep=";")

dpi.head(10)

# # Chargement TS preprocess

ts = pd.read_csv(interim_data + "declarations_concatenees.csv.gz", sep=";")

ts.head(10)

# # Matching

# Definition mask sur DPI identifiant et groupe non nul
dpi = dpi[~pd.isnull(dpi["identifiant"]) & ~pd.isnull(dpi["TS_groupe"])]

# +
dpi["identifiant"] = dpi["identifiant"].apply(lambda x: str(int(x)))


dpi_ts_join_on_id_groupe = pd.merge(dpi,
                       ts[["entreprise_émmetrice", "identifiant"]].rename(
                           columns={"entreprise_émmetrice": "TS_groupe"}).drop_duplicates(),
                      on=["identifiant", "TS_groupe"],
                      how="inner")

dpi_ts_join_on_groupe = pd.merge(dpi,
                       ts[["entreprise_émmetrice"]].rename(
                           columns={"entreprise_émmetrice": "TS_groupe"}).drop_duplicates(),
                      on="TS_groupe",
                      how="inner")
# -

diff = dpi_ts_join_on_groupe[~dpi_ts_join_on_groupe["numero_dpi"].isin(
    dpi_ts_join_on_id_groupe["numero_dpi"])]

len(diff.drop_duplicates())

diff.describe()

diff["montant_remuneration_declarant_valeur"].sum()

diff.sort_values(by="montant_remuneration_declarant_valeur", ascending=False, inplace=True)
diff.head(200)
