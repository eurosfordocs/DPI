# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

# +
import os

os.chdir('..')

from IPython.core.interactiveshell import InteractiveShell

InteractiveShell.ast_node_interactivity = "all"
# -

# # Import & Read

import pandas as pd

pd.options.display.max_columns = 100

# +
from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.constants.files import *

from src.utils import *

dpi_file = DPI_ONLY_NAME
# dpi_file = DPI_NO_NAME
# dpi_file = DPI_FULL
# dpi_file = DPI_NO_NAME_SAMPLE
# -

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
raw_professionnels = os.path.join(RAW_DATA_DIR, 'professionnel_sante.csv')
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)

# # Création du fichier de DPI intermédiaire

download_file_from_url(COLUMNS_SIGNIFICATION_URL, COLUMNS_SIGNIFICATION_PATH)

create_interim_dpi(dpi_file)

df = pd.read_csv(interim_dpi, sep=';')
df.shape
df.head(3)

df.info()

df_dpi = df.dropna(subset=['nom_declarant', 'prenom_declarant'])
df_dpi.loc[:, 'nom_declarant'] = df['nom_declarant'].apply(lambda x: normalize_string(x))
df_dpi.loc[:, 'prenom_declarant'] = df['prenom_declarant'].apply(lambda x: normalize_string(x))
df_dpi.shape
df_dpi.head()

# # RPPS matching

# ## Import annuaire & remove homonymes

df_professionnels = pd.read_csv(raw_professionnels, sep=';')
df_professionnels = df_professionnels[df_professionnels['type_identifiant'] == 'RPPS']
df_professionnels = df_professionnels.dropna(subset=['nom', 'prénom'])
df_professionnels.to_csv('data/raw/professionnel_sante_rpps.csv', sep=';', index=False)

df_professionnels.shape
df_professionnels.head()


df_professionnels = pd.read_csv(raw_professionnels, sep=';')
df_professionnels = df_professionnels.rename(columns={'prénom': 'prenom', 'profession': 'profession_annuaire'})
df_professionnels = df_professionnels.dropna(subset=['nom', 'prenom'])
df_professionnels.shape
df_professionnels.head()

df_professionnels.columns

df_professionnels = df_professionnels[['nom', 'prenom', 'profession_annuaire', 'identifiant', 'type_identifiant']]
# Drop homonyme
df_professionnels = df_professionnels.drop_duplicates(subset=['nom', 'prenom'], keep=False)
# Keep RPPS
df_professionnels = df_professionnels[df_professionnels['type_identifiant'] == 'RPPS']
df_professionnels.loc[:, 'nom'] = df_professionnels['nom'].apply(lambda x: normalize_string(x))
df_professionnels.loc[:, 'prenom'] = df_professionnels['prenom'].apply(lambda x: normalize_string(x))
df_professionnels.shape
df_professionnels.head()

# ## Match dpi

df_dpi_with_id = pd.merge(
    df_dpi,
    df_professionnels,
    left_on=['nom_declarant', 'prenom_declarant'],
    right_on=['nom', 'prenom'],
    how='left'
)

df_dpi_with_id.head()

# # Analyse

# ## Prévisualisation valeurs
# Prend aléatoirement des valeurs non nulles dans les colonnes

def preview_not_nan(df, n=5):
    preview_columns = dict()
    for column in df.columns:
        preview_columns[column] = (df[column].fillna('nan')
                                   .drop_duplicates()
                                   .sample(n, replace=True)
                                   .reset_index(drop=True)
                                   )
    return pd.DataFrame(preview_columns)


preview_not_nan(df)

# ## Compte distinct valeurs

df["nom_prenom_declarant"] = df.nom_declarant + '_' + df.prenom_declarant

df.nom_prenom_declarant.nunique()

df.rubrique_lien.value_counts().sort_index()

df.institution.nunique()

df.numero_dpi.nunique()

# ## Recherche colonnes avec montant

df.instance = df.instance.str.replace('â€™', "''")

for col in df.columns:
    mask = (df[col].map(str).str.upper().str.contains("€") |
            df[col].map(str).str.upper().str.contains("EURO ") |
            df[col].map(str).str.upper().str.contains("EUROS ")
            )
    if mask.any():
        print(col)
        print(df[mask][col].head())

# + {"jupyter": {"outputs_hidden": true}}

# -


