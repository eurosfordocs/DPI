# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

# +
import os

os.chdir('..')

from tqdm.autonotebook import tqdm
from IPython.core.interactiveshell import InteractiveShell

InteractiveShell.ast_node_interactivity = "all"
# -

# # Import & Read

import pandas as pd
import numpy as np

pd.options.display.max_columns = 100
pd.options.display.max_rows = 200

# +
from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.constants.files import *

from src.utils import *

dpi_file = DPI_ONLY_NAME
# dpi_file = DPI_NO_NAME
dpi_file = DPI_FILE
# dpi_file = DPI_NO_NAME_SAMPLE
# -

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
raw_professionnels = os.path.join(RAW_DATA_DIR, 'professionnel_sante.csv')
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)

# # Création du fichier de DPI intermédiaire

download_file_from_url(COLUMNS_SIGNIFICATION_URL, COLUMNS_SIGNIFICATION_PATH)

create_interim_dpi(dpi_file)

# # RPPS matching

# L'objectif est de matcher tous les numéro DPI avec des identifiants. Une ligne est caractérisée par (nom_declarant, prenom_declarant, numero_dpi). On garde donc les triplets uniques.

# ## Process DPI file

# ### Import DPI file, clean rows with IDs, drop duplicates numero_dpi

# +
df_raw = pd.read_csv(interim_dpi, sep=';')
print('Raw DF shape: ', df_raw.shape)
df_raw_null = df_raw.copy()  # df_raw[df_raw['identifiant'].isnull()].copy()
df_raw_null = df_raw_null[df_raw_null['rubrique_lien'] == '§ 1. Activité Principale']
df_raw_null.loc[:, 'fonction_occupee'] = df_raw_null['fonction_occupee'].apply(normalize_string)
df_raw_null = df_raw_null.dropna(subset=['fonction_occupee'])
print('Null DF shape (rows to match left, keeping first rubrique): ', df_raw_null.shape)
df = df_raw_null.copy()
# Drop duplicates based on dpi number, then on fonction_occupee
df = df.drop_duplicates(subset=['numero_dpi'])
# df = df.drop_duplicates(subset=['nom_declarant', 'prenom_declarant', 'fonction_occupee'])
# df = df.drop_duplicates(subset=['nom_declarant', 'prenom_declarant', 'fonction_occupee'])
# df = df.drop_duplicates(subset=['nom_declarant', 'prenom_declarant', 'numero_dpi'])
# df = df[(~df.duplicated(subset=['identifiant'])) | (df['identifiant'].isnull())]

# df = df[['nom_declarant', 'prenom_declarant', 'fonction_occupee', 'fonction_declarant', 'numero_dpi']]
print('DF without duplicates numero_dpi/fonction_occupee shape: ', df.shape)
df.sort_values(by=['nom_declarant', 'prenom_declarant']).head(50)
# -

# Le matching va se faire en utilisant le triplet (nom_declarant,	prenom_declarant, fonction_normalisee) avec fonction_normalisee une normalisation de fonction_occupee. On garde le numéro dpi pour rematcher le DF original plus tard

# ## Divide healthcare persons and non healthcare

df['fonction_occupee'].value_counts()[900:900 + 100]

dic_sante = {
    'yes': [
        'praticien',
        'medecin',
        'puph',
        'pu ph',
        'pu-ph',
        'pu ',
        'ph ',
        'sanitaire',
        'sante',
        'chirurgi',
        'cardiolog',
        'service',
        'generaliste',
        'mcu',
        'mcu-ph',
        'psych',
        'epidemi',
        'pharma',
        'medical',
        'infirm',
        'intern',
        'sage',
        'sage femme',
        'radio',
        'dr ',
    ],
    'maybe': [
        'evaluateur',
        'professeur',
        'consultant',
        'etudiant',
        'retraite',
        'directeur',
        'maitre',
        'unite',
        'ingenieur',
        'responsable',
        'mission',
        'chef',
        'charge',
        'benevol',
        'chercheur',
        'secretaire',
        'technique',
        'president',
        'inserm',
    ],
}

# +
from difflib import get_close_matches


def get_reversed_dict(value_to_list):
    mapping = dict()
    for value, list_ in value_to_list.items():
        for list_element in list_:
            mapping[list_element] = value
    return mapping

dic_sante_reverse = get_reversed_dict(dic_sante)
print(dic_sante_reverse.keys())

for index, row in tqdm(df.iterrows()):
    match = [word for word in dic_sante_reverse.keys() if word in row['fonction_occupee']]
    if match:
        df.loc[index, 'sante'] = dic_sante_reverse[match[0]]


# -

df['sante'].value_counts()

foo = df.copy()
for index, row in tqdm(foo.iterrows()):
    for key, l in dic_sante.items():
        for lem in l:
            if lem in row['fonction_occupee']:
                foo.loc[index, 'sante'] = key
foo.shape
foo.head(10)

foo['sante'].value_counts()

# +



def get_strict_mapper_to_category():
    modality_to_category = get_reversed_dict(dic_sante)
    for modality, category in modality_to_category.items():
        modality_to_category[modality] = category
    return defaultdict(lambda: ontology.UNSUCESSFULL_MAPPING, modality_to_category)


def get_similarity_mapper_to_category(s_detail):
    mapping = get_reversed_dict(dic_sante).copy()
    modalities = mapping.keys()
    print(modalities)
    for detail in s_detail.unique():
        match = get_close_matches(detail, modalities, n=1, cutoff=0.8)
        # print(match)
        if match:
            mapping[detail] = mapping[match[0]]
    return mapping


def get_category(df):
    mapping = get_similarity_mapper_to_category(df['fonction_occupee'])
    # print(mapping)
    result = df['fonction_occupee'].map(mapping)
    return result


# -

get_category(df).value_counts()

# + {"heading_collapsed": true, "cell_type": "markdown"}
# ### Normalize their functions and drop those whose function is not characterizable

# + {"hidden": true}
df['fonction_occupee'].value_counts()[0:0 + 50]


# + {"hidden": true}
# 4 catégories : médecin, sage-femme, pharmacien, autre
def normalize_fonction_dpi(fonction: str):
    if pd.isna(fonction):
        return np.nan
    if 'pharmacien' in fonction:
        return 'pharmacien'
    if 'sage ' in fonction:
        return 'sage femme'
    if 'technicien' in fonction:
        return 'autre'
    if 'kine' in fonction:
        return 'kine'
    if 'masseur' in fonction:
        return 'kine'
    if 'infirmier' in fonction:
        return 'infirmier'
    if 'professeur' in fonction or 'maitre' in fonction or 'enseignant' in fonction:
        return 'medecin'
    if 'directeur' in fonction or 'directrice' in fonction:
        return 'medecin'
    if 'responsable' in fonction:
        return 'medecin'
    if 'president' in fonction:
        return 'medecin'
    if 'praticien' in fonction:
        return 'medecin'
    if 'chercheur' in fonction:
        return 'medecin'
    if 'medecin' in fonction:
        return 'medecin'
    if 'chirurg' in fonction:
        return 'medecin'
    if 'radio' in fonction:
        return 'medecin'
    if 'psychiatre' in fonction:
        return 'medecin'
    if fonction in ['puph', 'ph', 'pu', 'pu-ph', 'mcu-ph',
                    'mcu'] or 'ph ' in fonction or 'pu-' in fonction or 'pu ' in fonction:
        return 'medecin'
    if 'chef' in fonction:
        return 'medecin'
    if 'dr' in fonction:
        return 'medecin'
    return 'autre'


# + {"hidden": true}
df['fonction_normalisee'] = df['fonction_occupee'].apply(normalize_fonction_dpi)
df = df.dropna(subset=['fonction_normalisee'])
df.shape
df.head()

# + {"hidden": true}
df.sort_values('numero_dpi').head(200)
# -

# ## Process annuaire

# ### Import annuaire & remove non homonymes

# +
from src.constants import columns as c


def preprocess_annuaire_pro(drop_non_rpps=True, keep_identifiant=False) -> pd.DataFrame:
    """
    Read and clean annuaire professionnels, keeping name, last_name and rpps-only id columns
    """
    nrows = int(os.getenv("NROWS", 10 ** 8))  # to speed up tests
    logging.debug("On limite le nombre de lignes à {} (pour les tests)".format(nrows))
    df_annuaire = pd.read_csv(PROFESSIONNELS_SANTE_PATH, sep=';', compression='gzip', low_memory=False,
                              usecols=['nom', 'prénom', 'type_identifiant', c.IDENTIFIANT_PROFESSIONNEL, 'profession',
                                       'specialité'],
                              nrows=nrows)

    df_annuaire = df_annuaire.dropna(subset=['nom', 'prénom'])
    df_annuaire[c.NOM_DECLARANT] = df_annuaire['nom'].map(normalize_string)
    df_annuaire[c.PRENOM_DECLARANT] = df_annuaire['prénom'].map(normalize_string)

    # Drop non RPPS
    if drop_non_rpps:
        df_annuaire = df_annuaire[df_annuaire['type_identifiant'] == 'RPPS']
    # Drop duplicates
    df_annuaire = df_annuaire.drop_duplicates(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], keep=False)
    # Keep duplicates
    ##df_annuaire = df_annuaire[df_annuaire.duplicated(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], keep=False)]

    if keep_identifiant:
        return df_annuaire.drop(columns=['nom', 'prénom'])
    else:
        return df_annuaire.drop(columns=['nom', 'prénom', 'type_identifiant'])


# -

df_professionnels_raw = preprocess_annuaire_pro(drop_non_rpps=False, keep_identifiant=True)
df_professionnels_raw.shape
df_professionnels_raw.head()

# Match exactly
df_result = pd.merge(
    df.drop(['identifiant'], axis=1),
    df_professionnels_raw[['nom_declarant', 'prenom_declarant', 'identifiant']],
    on=['nom_declarant', 'prenom_declarant'],
    how='left'
)

df_result[df_result['sante'] == 'no'].info()

# ### Normalize annuaire pro functions

df_professionnels_raw['profession'].unique()

normalize_fonction_annuaire_dic = {
    'Audioprothésiste': 'autre',
    'Opticien-Lunetier': 'autre',
    'Infirmier': 'infirmier',
    'Ergothérapeute': 'autre',
    'Orthophoniste': 'autre',
    'Technicien de laboratoire médical': 'autre',
    'Diététicien': 'medecin',
    'Podo-Orthésiste': 'medecin',
    'Psychomotricien': 'autre',
    'Orthoprothésiste': 'autre',
    'Orthopédiste-Orthésiste': 'medecin',
    'Orthoptiste': 'medecin',
    'Sage-Femme': 'sage femme',
    'Médecin': 'medecin',
    'Pharmacien': 'pharmacien',
    'Chirurgien-Dentiste': 'medecin',
    'Masseur-Kinésithérapeute': 'kine',
    'Pédicure-Podologue': 'medecin',
    'Epithésiste': 'autre',
    'Oculariste': 'autre',
}
df_professionnels_raw['normalized_profession'] = df_professionnels_raw['profession'].replace(
    normalize_fonction_annuaire_dic)
df_professionnels_raw.shape
df_professionnels_raw.head()
df_professionnels = df_professionnels_raw.copy()

# ### Drop duplicates from annuaire based on unique triplet in order to discard those which are impossible to identify

df_professionnels = df_professionnels.drop_duplicates(
    subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, 'normalized_profession'], keep=False)
df_professionnels.shape

# ## Get dpi quadruplets which can match with anyone in annuaire based on people name

df_professionnels_temp = df_professionnels[['nom_declarant', 'prenom_declarant']].drop_duplicates()
df_professionnels_temp.shape

df_clean = pd.merge(
    df,
    df_professionnels_temp,
    on=['nom_declarant', 'prenom_declarant'],
    how='inner',
)
df_clean.shape
print("There are {} matchable rows.".format(len(df_clean)))
df_clean.head(20)

# ## Merge both

df_professionnels.shape
df_professionnels.head()

df_with_id = pd.merge(
    df_clean,
    df_professionnels,
    left_on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, 'fonction_normalisee'],
    right_on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, 'normalized_profession'],
    how='inner',
).drop_duplicates(subset=['numero_dpi', 'identifiant'])
df_with_id['type_identifiant'].value_counts()
df_with_id.shape
df_with_id.head()

df_with_id.to_csv('dpi_experts_to_analyze.csv', index=False)

# ## Drop rows with duplicate numero_dpi but different identifiant

df_with_id_clean = df_with_id.groupby('numero_dpi').filter(
    lambda g: len(g['identifiant'].unique()) == 1).drop_duplicates(subset=['numero_dpi', 'identifiant'])
df_with_id_clean.shape

# ## Evaluate number of matched rows

df_raw_null[df_raw_null['numero_dpi'].isin(df_with_id['numero_dpi'])].shape

# ### Check some matched people

df_raw_null.head()
df_raw_null.info()

df_with_id.head()
df_with_id.info()

nom_declarant = 'huot'
prenom_declarant = 'olivier'
df_professionnels_raw[(df_professionnels_raw[c.NOM_DECLARANT] == nom_declarant) & (
            df_professionnels_raw[c.PRENOM_DECLARANT] == prenom_declarant)]

# ## Match back dpi

df_raw_null.head()

len(df_with_id_clean.apply(lambda x: '{}_{}'.format(x['numero_dpi'], x['identifiant']), axis=1).unique())

df_dpi_new = pd.merge(
    df_raw_null.drop(['identifiant'], axis=1),
    df_with_id_clean[['numero_dpi', 'identifiant']].drop_duplicates(),
    on='numero_dpi',
    how='left'
)
df_dpi_new.shape
df_dpi_new.head()

df_dpi_new.info()
