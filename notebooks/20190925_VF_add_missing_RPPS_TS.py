# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

import os
os.chdir('..')

# # Import & Read

import pandas as pd
pd.options.display.max_columns=100


TS_PATH = 'data/raw/declaration_remuneration.csv.gz'
df = pd.read_csv(TS_PATH, sep=';',compression='gzip')
df.head(3)

# # Analyse RPPS

# Nombre total d'identifiants
len(df.identifiant)

# Nombre total d'identifiants nuls
df.identifiant.isna().sum()

# Pourcentage de manquants
df.identifiant.isna().sum()/len(df.identifiant)

# # Chargement BDD RPPS

PROFESSIONNELS_SANTE_PATH = "data/raw/professionnel_sante.csv.gz"

df_annuaire = pd.read_csv(PROFESSIONNELS_SANTE_PATH, sep=';', compression='gzip', low_memory=False,
                              usecols=['nom', 'prénom', 'type_identifiant', 'identifiant'],
                              nrows=10**8)

df_annuaire.head(50)

# # Extraction Numéro RPPS

# Drop Nan
df_annuaire = df_annuaire.dropna(subset=['nom', 'prénom'])

# +
from src.utils import normalize_string

df_annuaire['nom_declarant'] = df_annuaire['nom'].map(normalize_string)
df_annuaire['prenom_declarant'] = df_annuaire['prénom'].map(normalize_string)

df_annuaire = df_annuaire.drop_duplicates(subset=['nom_declarant', 'prenom_declarant'], keep=False)
# -

# # Extraction RPPS dans base TS

# +
subset_df = df.dropna(subset=['nom', 'prénom']).copy()
mask = subset_df.identifiant.isna()
subset_df = subset_df[mask]

subset_df['nom_declarant'] = subset_df['nom'].map(normalize_string)
subset_df['prenom_declarant'] = subset_df['prénom'].map(normalize_string)
# -

subset_df.head(100)

mask_null = df['prénom'].notnull() & df['nom'].notnull() & df['identifiant'].isna()

index_copy = subset_df.index
subset_df = subset_df.merge(df_annuaire, on=['nom_declarant', 'prenom_declarant'], how='left')
subset_df.index = index_copy

df.loc[mask_null,'identifiant'] = subset_df['identifiant_y']
df.loc[mask_null,'type_identifiant'] = subset_df['type_identifiant_y']

df[mask_null]

# # Analyse Résultats

# Nombres d'identifiants manquants avec prénom et nom
len(df[mask_null]['identifiant'].notnull())

# Nombres de valeurs retrouvées
sum(df[mask_null]['identifiant'].notnull())