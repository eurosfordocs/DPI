# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

import os
os.chdir('..')

# # Import & Read

import pandas as pd
pd.options.display.max_columns=100
pd.options.display.max_rows=100

from src.constants.files import *
from src.utils import *

dpi_file = DPI_ONLY_NAME
dpi_file = DPI_NO_NAME
dpi_file = DPI_FILE
#dpi_file = DPI_NO_NAME_SAMPLE

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)

# # Création du fichier de DPI intermédiaire

download_all_files()

# create_interim_dpi(dpi_file)

df = pd.read_csv(interim_dpi, sep=';')
df.head(3)

# # Analyse

# ## Prévisualisation valeurs
# Prend aléatoirement des valeurs non nulles dans les colonnes

def preview_not_nan(df, n=5):
    preview_columns = dict()
    for column in df.columns:
        preview_columns[column] = (df[column].fillna('nan')
                                   .drop_duplicates()
                                   .sample(n, replace=True)
                                   .reset_index(drop=True)
                                  )
    return pd.DataFrame(preview_columns)

preview_not_nan(df)

# ## Compte distinct valeurs

df_entreprise = pd.read_csv(os.path.join(RAW_DATA_DIR, "entreprise.csv"), sep=',')

df_association = pd.read_csv(os.path.join(RAW_DATA_DIR, "association_entreprise_groupe.csv"), sep=',')

df_association.head(2)

df_entreprise.denomination_sociale.map(normalize_string).sample(3)

sdf = (df
 .organisme_emetteur.dropna()
 .map(normalize_string)
 .value_counts().reset_index()
)
sdf.columns = ['organisme_emetteur', 'nb_occurences']
sdf = sdf[['nb_occurences', 'organisme_emetteur']]


s_percent = (100 * sdf.nb_occurences.cumsum() / sdf.nb_occurences.sum()).round(2)
s_percent.name = "cum_percent"
sdf = pd.concat([s_percent, sdf], axis=1)

sdf.to_csv('organismes.csv', index=False)

x= sdf.nb_occurences
x[x<30].hist(bins=100);

(x.cumsum().head(1000) / x.sum()).plot();

# + {"jupyter": {"outputs_hidden": true}, "cell_type": "markdown"}
# # Recréation du fichier DPI_TO_TS_ORGANISME 
# après renormalisation et suppression des doublons
# -

def normalize_string(s: str) -> str:
    if not isinstance(s, str):
        return s
    s = s.lower()
    s = unidecode(s)  # strip accents
    s = ' '.join(s.split())
    return s


df_organisme = pd.read_csv(DPI_TO_TS_ORGANISME_PATH)
df_organisme.organisme_emetteur = df_organisme.organisme_emetteur.map(normalize_string)
df_organisme = df_organisme.drop_duplicates(subset=["organisme_emetteur"])
df_organisme.to_csv('organismes.csv', index=False)

# # Association du groupe TS

from src.preprocess.create_interim_dpi.organisme import *

df_dpi = pd.read_csv(join(INTERIM_DATA_DIR, DPI_NO_NAME_SAMPLE), sep=';')

df_organisme = pd.read_csv(DPI_TO_TS_ORGANISME_PATH)[[c.ORGANISME, "TS_groupe"]]
df_organisme.head()


df_dpi.merge(df_organisme, how="left", on=c.ORGANISME)[[c.ORGANISME, "TS_groupe"]]

add_column_groupe_TS(df_dpi)[[c.ORGANISME, "TS_groupe"]].head(50)

df_organisme = pd.read_csv(DPI_TO_TS_ORGANISME_PATH)[[c.ORGANISME, "TS_filiale", "TS_groupe"]]
df_company_to_group = pd.read_csv(COMPANY_GROUP_PATH)[["denomination_sociale", "groupe"]].drop_duplicates()

df_company_to_group.groupe = df_company_to_group.groupe.fillna(df_company_to_group.denomination_sociale)

# +
df_tmp = df_organisme.merge(df_company_to_group, how="left", left_on="TS_filiale", right_on="denomination_sociale", validate="many_to_one")

mask = df_tmp.groupe.notna() & df_tmp.TS_groupe.notna()
assert (df_tmp.loc[mask, "TS_groupe"] == df_tmp.loc[mask, "groupe"]).all()
# -

mask = df_tmp.TS_filiale.notna() & df_tmp.TS_groupe.isna()
df_tmp.loc[mask, "TS_groupe"] = df_tmp.loc[mask, "groupe"]

# +
df_company_to_group = pd.read_csv(COMPANY_GROUP_PATH)[["denomination_sociale", "groupe"]].drop_duplicates()
# Fill groupe with denomination_sociale when company has no group (for independent ones)
df_company_to_group["groupe"] = df_company_to_group.groupe.fillna(df_company_to_group["denomination_sociale"])

df_organisme = pd.read_csv(DPI_TO_TS_ORGANISME_PATH)[[c.ORGANISME, "TS_filiale", "TS_groupe"]]
df_organisme = df_organisme.merge(df_company_to_group, how="left", left_on="TS_filiale",
                                  right_on="denomination_sociale", validate="many_to_one")

mask_remplissage_filiale_ET_groupe = df_organisme["TS_filiale"].notna() & df_organisme["TS_groupe"].notna()
assert (df_organisme.loc[mask_remplissage_filiale_ET_groupe, "TS_groupe"] ==
        df_organisme.loc[mask_remplissage_filiale_ET_groupe, "groupe"]).all()

mask_filiale_PAS_groupe = df_organisme["TS_filiale"].notna() & df_organisme["TS_groupe"].isna()
df_organisme.loc[mask_filiale_PAS_groupe, "TS_groupe"] = df_organisme.loc[mask_filiale_PAS_groupe, "groupe"]
# -


