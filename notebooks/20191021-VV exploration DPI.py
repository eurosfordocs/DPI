# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
# %matplotlib inline
pd.set_option("max_columns", 100)
pd.set_option("max_rows", 200)
pd.set_option('max_colwidth', 400)
from datetime import datetime
import sys
sys.path.append("..")
from src.utils import normalize_string

interim_data_path = "../data/interim/"
raw_data_path = "../data/raw/"

sign_cols = pd.read_csv(raw_data_path + "signification_colonnes.csv", sep=",").drop(
    ["Page manuel", "Nb lignes"], axis=1)
sign_cols.head(1)

# +
sign_cols_dict = {}

for index, row in sign_cols.iterrows():
    detail_liens_list = [row["detail_lien_{}".format(str(x))] for x in range(1, 11)
                         if row["detail_lien_{}".format(str(x))] not in ["VIDE", "date_debut_fin"]]
    for montant_col in ["montant_remuneration_organisme", "montant_remuneration_declarant"]:
        if montant_col in detail_liens_list:
            detail_liens_list += [montant_col + "_" + suf for suf in ["valeur", "devise", "frequence"]]
    if "organisme_financeur_montant_pourcentage" in detail_liens_list:
        detail_liens_list += ["organisme_financeur", "montant_activites_financees", "pourcentage_activites_financees"]
    
    detail_liens_list += ["date_debut", "date_fin", "numero_dpi", "nom_prenom_declarant"]
    sign_cols_dict[row["rubrique_lien"]] = detail_liens_list
    
sign_cols_dict
# -

dpi = pd.read_csv(interim_data_path + "20190903001500_export_declarations_publiees.csv", sep=";",
                 parse_dates=["date_debut", "date_fin"])

len(pd.unique(dpi["numero_dpi"]))

# +
# Comptage du nombre de rubriques distinctes par numéro dpi

dpi_rubr_counts = dpi.groupby(["numero_dpi"], as_index=False).agg(
                    {"rubrique_lien": lambda x: len(set(x))}).rename(columns={"rubrique_lien": "nb_rubriques"})
dpi_rubr_counts[dpi_rubr_counts["nb_rubriques"]<10]["nb_rubriques"].value_counts()
# -

dpi_1_rubr = dpi[dpi["numero_dpi"].isin(dpi_rubr_counts[dpi_rubr_counts["nb_rubriques"]==1]["numero_dpi"])]
dpi_1_rubr["rubrique_lien"].value_counts()

# 15455 DPI sur 29190 ne renseignent que la rubrique "activité principale". On peut les filtrer.

dpi_f = dpi[~dpi["numero_dpi"].isin(dpi_rubr_counts[dpi_rubr_counts["nb_rubriques"]==1]["numero_dpi"])]

# Étude de l’impact du drop_duplicates sur chaque section
for rubrique in pd.unique(dpi_f["rubrique_lien"]):
    rubr_df = dpi_f[dpi_f["rubrique_lien"]==rubrique][sign_cols_dict[rubrique]]
    print(rubrique)
    print("len {} ==> len_dd {} ({}%)".format(
    len(rubr_df), len(rubr_df.drop_duplicates()), round(100*len(rubr_df.drop_duplicates())/len(rubr_df), 1)))

# +
dpi_dd = dpi_f.drop(["institution", "instance", "date_debut_instance", "date_fin_instance", "fonction_declarant",
                  "qualite", "statut_declaration", "date_debut_mandat", "date_fin_mandat"], axis=1).drop_duplicates()

print(len(dpi))
print(len(dpi_dd))

# +
# Nombre de lignes par dpi
nb_lignes_dpi = dpi_dd.groupby(["numero_dpi"]).agg({"nom_prenom_declarant": len}).rename(
columns={"nom_prenom_declarant": "nb_lignes"}).sort_values(by="nb_lignes", ascending=False)

nb_lignes_dpi.head()
# -

# # Étude des dpi rubrique par rubrique

rubriques = pd.unique(dpi_dd["rubrique_lien"])

# +
rubr_type_rem = []
rubr_pas_type_rem = []
for rubrique in rubriques:
    if "type_remuneration" in  sign_cols_dict[rubrique]:
        rubr_type_rem.append(rubrique)
    else:
        rubr_pas_type_rem.append(rubrique)

print("type rem")
for rubr in rubr_type_rem:
    print(rubr)

print("\npas type rem")
for rubr in rubr_pas_type_rem:
    print(rubr)
# -

# ## Filtre type rémunération

dpi_dd["type_remuneration"].value_counts()

# +
dpi_dd_rem = dpi_dd[dpi_dd["type_remuneration"]!="Aucune rémunération"]

print(len(dpi_dd_rem))
# -

# ## Filtre sur les rubriques pertinentes

# +
useless_rubr = ["§ 1. Activité Principale",
                "§ 5. Proches parents salariés et/ou possédant des intérêts financiers dans toute institution",
                "§ 7. Autres liens d'intérêts",
                "§ 4. Participations financières directes",
                "§ 6. Fonctions et mandats électifs"]

dpi_useful = dpi_dd_rem[~dpi_dd_rem["rubrique_lien"].isin(useless_rubr)].copy()

print(len(dpi_useful))
# -

# # Calcul de kpi de base sur les dpi intéressantes

len(pd.unique(dpi_useful["numero_dpi"]))

len(pd.unique(dpi_useful["organisme_financeur"]))

len(pd.unique(dpi["organisme_financeur"]))

# On a divisé par 3 le nombre d’organismes à matcher.

len(pd.unique(dpi_useful["nom_prenom_declarant"]))

len(pd.unique(dpi["nom_prenom_declarant"]))

# KPIs intéressants à suivre:
# - Nombre de dpi total
# - Nombre de dpi concernant des montants potentiellement déclarés dans TS.
# - Nombre des entreprises concernées par ces montants.
# - Nombre des personnes concernées par ces montants.
# - Pourcentage d’entreprises matchées à un groupe TS.
# - Pourcentage de personnes matchées à un numéro RPPS.
# - Pourcentage des participations de la rubrique "Activités financées par un organisme à but lucratif" correctement parsées.
# - Somme des montants de la rubrique 3 correctement parsés (M€).
# - Somme des montants de la rubrique 3 incorrectement parsés (M€).
# - Somme des montants de la rubrique 2 organisme (M€).
# - Somme des montants de la rubrique 2 déclarant (M€).

# +
def compute_perc(col_1, col_2):
    perc = str(round(100*len(pd.unique(dpi_useful[col_1]))
                           / len(pd.unique(dpi_useful[col_2])), 2)) + "%"

nb_dpi_tot = len(pd.unique(dpi["numero_dpi"]))
nb_dpi_useful = len(pd.unique(dpi_useful["numero_dpi"]))
nb_companies = len(pd.unique(dpi_useful["organisme_financeur"]))
nb_people = len(pd.unique(dpi_useful["nom_prenom_declarant"]))
perc_company_match = compute_perc("TS_groupe", "organisme_financeur")
perc_people_match = compute_perc("identifiant", "nom_prenom_declarant")
#TODO: Pourcentage d’homonymes et d’homonymes matchés ?
#TODO: Pourcentage des participations de la rubrique "Activités financées par un organisme à but lucratif" correctement parsées.
#TODO: Somme des montants de la rubrique 3 correctement parsés (M€).
#TODO: Somme des montants de la rubrique 3 incorrectement parsés (M€).
# -

dpi_rem = dpi_useful[dpi_useful["rubrique_lien"].apply(lambda x: x[:3])=="§ 2"].copy().reset_index(drop=True)

# +
from src.preprocess.create_interim_dpi.extract_montant import extract_montant

dpi_rem = extract_montant(dpi_rem)
# -


dpi_rem["montant_remuneration_declarant_devise"].value_counts()

# +
recipient_list = ["declarant", "organisme"]

for recipient in recipient_list:
    dpi_rem["montant_remuneration_{}_total".format(recipient)] = 0
    dpi_rem["date_fin_no_na"] = dpi_rem["date_fin"].fillna(datetime.now())

    for index, row in dpi_rem.iterrows():
        if row["montant_remuneration_{}_frequence".format(recipient)] == "Total":
            dpi_rem.at[index, "montant_remuneration_{}_total".format(recipient)] = float(row[
                "montant_remuneration_{}_valeur".format(recipient)])
        elif row["montant_remuneration_{}_frequence".format(recipient)] == "Annuel":
            nb_years = (row["date_fin_no_na"] - row["date_debut"]).days / 365
            dpi_rem.at[index, "montant_remuneration_{}_total".format(recipient)] = float(row[
                "montant_remuneration_{}_valeur".format(recipient)]) * nb_years
        elif row["montant_remuneration_{}_frequence".format(recipient)] == "Mensuel":
            nb_months = (row["date_fin_no_na"] - row["date_debut"]).days / (365/12)
            dpi_rem.at[index, "montant_remuneration_{}_total".format(recipient)] = float(row[
                "montant_remuneration_{}_valeur".format(recipient)]) * nb_months

dpi_rem.drop("date_fin_no_na", axis=1, inplace=True)

# +
# Conversion de toutes les devises en euros
conversion_rates = {"euros": 1, "dollars": 0.9, "chf": 0.9, "dollars canadiens": 0.7, "dollars americain": 0.9,
                   "livre sterling": 1.2, "dollars americains": 0.9}

recipient_list = ["declarant", "organisme"]

for recipient in recipient_list:
    for index, row in dpi_rem.iterrows():
        devise = normalize_string(str(row["montant_remuneration_{}_devise".format(recipient)]))
        if devise != "none":
            try:
                dpi_rem.at[index, "montant_remuneration_{}_total_euros".format(recipient)] = (
                    row["montant_remuneration_{}_total".format(recipient)]
                    * conversion_rates[devise])
            except:
                raise ValueError("La devise {} n’est pas dans le dictionnaire de conversion.".format(devise))

# +
sum_amounts_rubr_2_organisme = dpi_rem["montant_remuneration_organisme_total_euros"].sum()
sum_amounts_rubr_2_declarant = dpi_rem["montant_remuneration_declarant_total_euros"].sum()

print(sum_amounts_rubr_2_declarant, sum_amounts_rubr_2_organisme)
# -

# # Focus autres liens d’intérêts (pas pris en compte car données trop destructurées)

# +
rubr_7 = "§ 7. Autres liens d'intérêts"

dpi_rubr_7 = dpi_dd_rem[dpi_dd_rem["rubrique_lien"]==rubr_7][sign_cols_dict[rubr_7]]

print(len(dpi_rubr_7))

dpi_rubr_7.head(100)
# -

# ### TODO Capture du montant_lien_intérêt ? Données très sales

# # Focus activités financées par un organisme à but lucratif

# +
rubr_3 = "§ 3. Activités financées par un organisme à but lucratif"

dpi_rubr_3 = dpi_dd_rem[dpi_dd_rem["rubrique_lien"]==rubr_3][sign_cols_dict[rubr_3]]

print(len(dpi_rubr_3))

dpi_rubr_3.head(100)
# -

dpi_rubr_3["montant_activites_financees"].dropna().apply(
    lambda x: int(str(x).replace(".", ",").split(",")[0])).sum()

# Comparaison avec les rémunérations organisme et déclarant

dpi_dd_rem[
    dpi_dd_rem["montant_remuneration_organisme_valeur"]!="Montant"][
    "montant_remuneration_organisme_valeur"].dropna().apply(int).sum()

dpi_dd_rem[
    dpi_dd_rem["montant_remuneration_organisme_valeur"]!="Montant"][
    "montant_remuneration_declarant_valeur"].dropna().apply(int).sum()

# # Focus Participations financières directes (pas pertinentes car non déclarées dans TS)

# +
rubr_4 = "§ 4. Participations financières directes"

dpi_rubr_4 = dpi_dd_rem[dpi_dd_rem["rubrique_lien"]==rubr_4][sign_cols_dict[rubr_4]]

print(len(dpi_rubr_4))

dpi_rubr_4.head(100)
# -


