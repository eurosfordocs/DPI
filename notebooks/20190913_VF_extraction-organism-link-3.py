# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

import os
os.chdir('..')

# # Import & Read

import pandas as pd
pd.options.display.max_columns=50


data_path = "data/raw/dpi_no_name.csv"

header = [
    'institution',
    'instance',
    'date_debut_instance',
    'date_fin_instance',
    'nom_declarant',
    'prenom_declarant',
    'fonction_declarant',
    'qualite',
    'statut_declaration',
    'date_debut_mandat',
    'date_fin_mandat',
    'numero_dpi',
    'date_soumission_dpi',
    'rubrique_lien',
    'detail_lien_1',
    'detail_lien_2',
    'detail_lien_3',
    'detail_lien_4',
    'detail_lien_5',
    'detail_lien_6',
    'detail_lien_7',
    'detail_lien_8',
    'detail_lien_9',
    'detail_lien_10',
    'detail_lien_11'
]

df = pd.read_csv(data_path, sep=';', skiprows=1, header=0, names=header)
df.head(3)

# # Extraction Organism + Montant Link 3

# ## Visualisation

extract_df = df[df['rubrique_lien'] == '§ 3. Activités financées par un organisme à but lucratif']['detail_lien_3']

# + {"jupyter": {"outputs_hidden": true}}
extract_df.head(10)
# -

# ## Détermination du pourcentage de la participation financière

# Ensemble des patterns définis pour l'instant : 
#
# - 1% 
# - 1.4%
# - 1.4% ... 1.4% ... 1.5%
# - 1,4%
# - 1,4 %

# +
import re

# Détermination de l'ensemble des lignes en désaccord avec le pattern

no_matching_rows = []

for row in extract_df.index.values :
    match = re.findall('\d+\.?,?\d*\s*%',extract_df[row])
    if len(match)==0:
        no_matching_rows.append(row)
# -

# Display des lignes en désaccord avec le matching 
extract_df[no_matching_rows]

# Détermination si un pourcentage est présent dans la liste des non matching rows
for row in no_matching_rows: 
    if len(re.findall('%',extract_df[row]))>0:
        print(extract_df[row])

# ## Détermination du montant de la participation financière

# Recherche du montant avant la participation. L'ensemble des patterns défini pour l'instant : 
#
# - 22432
# - 27520,72€

# +
# Décomposition de chaque ligne en fonction du pourcentage d'intéressement 

string_split = {}

for row in extract_df.index.values :
    # Détermination des pourcentages d'intéressement
    matches = re.findall('\d+\.?,?\d*\s*%',extract_df[row])
    # Décomposition de la ligne en fonction des pourcentages 
    string = extract_df[row]
    string_split[row] = []
    for match in matches : 
        split = string.split(match)
        string_split[row].append(split[0])
        if len(split)>1:
            string = split[1]
# -

# Affichage de la décomposition
print(string_split)

# +
# Détermination de l'ensemble des lignes en désaccord avec le pattern
no_matching_rows = []

for row in extract_df.index.values :
    for split in string_split[row] : 
        match = re.findall('\s\d+\.?\d+\.?\d+\.?,?\d*€?\s',split)
        if len(match)>1:
            no_matching_rows.append(row)
# -

# Display des lignes en désaccord avec le matching 
extract_df[no_matching_rows]

# ## Extract organism, montant, percentage

# +
# Definition de la fonction

def extract_organism_montant_percentage(extract_df) : 
    
    information_dict = {}
    for row in extract_df.index.values :
        information_dict[row] = ['NotExtracted','NotExtracted','NotExtracted']
    
    # Extraction percentage
    for row in extract_df.index.values :
        matches = re.findall('\d+\.?,?\d*\s*%',extract_df[row])
        if len(matches)==1:
            information_dict[row][2] = matches[0]
            split = extract_df[row].split(matches[0])[0]
            match = re.findall('\s\d*\.?\s?\d*\.?\s?\d+\.?,?\d*',split)
            if len(match) == 1:
                information_dict[row][1] = match[0]
                information_dict[row][0] = split.split(match[0])[0]
            elif len(match) == 0:
                information_dict[row][0] = split
                
        elif len(matches)==0:
            information_dict[row][2] = 'NotExisting'
            match = re.findall('\s\d*\.?\s?\d*\.?\s?\d+\.?,?\d*',extract_df[row])
            if len(match) == 1:
                information_dict[row][1] = match[0]
                information_dict[row][0] = extract_df[row].split(match[0])[0]
            elif len(match) == 0:
                information_dict[row][0] = extract_df[row]
                
    return information_dict

# -

information = extract_organism_montant_percentage(extract_df)

information

# Calcul du nombre d'extraction
valid = 0
for key in information.keys():
    if information[key][0] is not 'NotExtracted' : 
        valid += 1
print(valid/len(information.keys()))
