# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

import os
os.chdir('..')

# # Import & Read

import pandas as pd
pd.options.display.max_columns=100


from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.constants.files import *
from src.utils import *

dpi_file = DPI_ONLY_NAME
dpi_file = DPI_NO_NAME
dpi_file = DPI_FILE
dpi_file = DPI_NO_NAME_SAMPLE

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)

# # Création du fichier de DPI intermédiaire

download_file_from_url(COLUMNS_SIGNIFICATION_URL, COLUMNS_SIGNIFICATION_PATH)

create_interim_dpi(dpi_file)

df = pd.read_csv(interim_dpi, sep=';')
df.head(3)

# # Analyse

# ## Prévisualisation valeurs
# Prend aléatoirement des valeurs non nulles dans les colonnes

def preview_not_nan(df, n=5):
    preview_columns = dict()
    for column in df.columns:
        preview_columns[column] = (df[column].fillna('nan')
                                   .drop_duplicates()
                                   .sample(n, replace=True)
                                   .reset_index(drop=True)
                                  )
    return pd.DataFrame(preview_columns)

preview_not_nan(df)

# ## Compte distinct valeurs

df["nom_prenom_declarant"] = df.nom_declarant + '_' + df.prenom_declarant

df.nom_prenom_declarant.nunique()

df.rubrique_lien.value_counts().sort_index()

df.institution.nunique()

df.numero_dpi.nunique()

# ## Recherche colonnes avec montant

df.instance = df.instance.str.replace('â€™', "''")

for col in df.columns:
    mask = (df[col].map(str).str.upper().str.contains("€") |
            df[col].map(str).str.upper().str.contains("EURO ") |
            df[col].map(str).str.upper().str.contains("EUROS ")
           )
    if mask.any():
        print(col)
        print(df[mask][col].head())

# + {"jupyter": {"outputs_hidden": true}}

