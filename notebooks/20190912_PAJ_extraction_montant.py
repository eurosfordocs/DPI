# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# %matplotlib inline

import os
os.chdir('..')

# # Import & Read

import pandas as pd
pd.options.display.max_columns=100


from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.constants.files import *

dpi_file = DPI_NO_NAME_SAMPLE
dpi_file = DPI_ONLY_NAME
dpi_file = DPI_NO_NAME
dpi_file = DPI_FILE

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)

# # Création du fichier de DPI intermédiaire 

create_interim_dpi(dpi_file)

df = pd.read_csv(interim_dpi, sep=';')
df.head(3)

# # Extraction colonnes

# ## Prévisualisation valeurs
# Prend aléatoirement des valeurs non nulles dans les colonnes

def preview_not_nan(df, n=5):
    preview_columns = dict()
    for column in df.columns:
        preview_columns[column] = (df[column].fillna('nan')
                                   .drop_duplicates()
                                   .sample(n, replace=True)
                                   .reset_index(drop=True)
                                  )
    return pd.DataFrame(preview_columns)

preview_not_nan(df)

mask = df.montant.fillna('').str.split().map(len) == 3
df['montant_valeur'] = pd.np.nan
df['montant_devise'] = pd.np.nan
df['montant_frequence'] = pd.np.nan
df.loc[mask, ['montant_valeur', 'montant_devise', 'montant_frequence']] = df[mask].montant.str.split().values.tolist()

montant_pattern = "(?P<{}>\d+) (?P<{}>.*) (?P<{}>Total|Annuel|Mensuel)".format('montant_valeur', 'montant_devise', 'montant_frequence')
df.montant_declarant.dropna().str.extract(montant_pattern)

for column in df.columns:
    if column.startswith('montant_'):
        montant_type = column[8:]
        montant_pattern = ("(?P<{}>\d+) (?P<{}>.*) (?P<{}>Total|Annuel|Mensuel)"
                           .format(column + '_valeur', column + '_devise', column + '_frequence')
                          )
        df = df.join(df[column].fillna('').str.extract(montant_pattern))


df.montant_remuneration_declarant.dropna()

montant_pattern

df['montant_remuneration_declarant'].dropna().str.extract(montant_pattern)

df.montant_capital.dropna()

preview_not_nan(df)

for i in range(1, 5):    
    mask = df.montant_remuneration_organisme.fillna('').str.split().map(len) == i
    print(i)
    print(df[mask].rubrique_lien.unique())
    print(df[mask].montant_remuneration_organisme.head())

# ## Recherche colonnes avec montant

df.instance = df.instance.str.replace('â€™', "''")

for col in df.columns:
    mask = (df[col].map(str).str.upper().str.contains("€") |
            df[col].map(str).str.upper().str.contains("EURO ") |
            df[col].map(str).str.upper().str.contains("EUROS ")
           )
    if mask.any():
        print(col)
        print(df[mask][col].head())

# # Analyse

df["nom_prenom_declarant"] = df.nom_declarant + '_' + df.prenom_declarant

df.rubrique_lien.value_counts().sort_index()

df.institution.nunique()

df.numero_dpi.nunique()

df.nom_prenom_declarant.nunique()

# + {"jupyter": {"outputs_hidden": true}}

