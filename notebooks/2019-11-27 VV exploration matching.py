# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import sys
sys.path.append("..")

# %matplotlib inline
pd.set_option("max_columns", 100)
pd.set_option("max_rows", 500)
pd.set_option('max_colwidth', 400)

import src.constants.columns as c
import src.constants.files as f

import os

from src import utils
# -

sign_col_dict = utils.sign_cols_dict()

rubr_to_drop = [rubr for rubr in sign_col_dict.keys() if rubr[2]!="2"]

diff_dpi_ts = pd.read_csv(os.path.join(f.RESULT_DATA_DIR, "{}_{}.csv".format(f.DIFF_DPI_TS, f.FULL)), sep=";")
diff_dpi_ts = diff_dpi_ts.drop("index", 1).drop_duplicates()

diff_dpi_dc = diff_dpi_ts.copy()
for rubr in rubr_to_drop:
    diff_dpi_dc.drop(list(set(sign_col_dict[rubr]).intersection(set(diff_dpi_dc.columns))), axis=1, inplace=True)

diff_dpi_dc.sort_values(by="montant_remuneration_declarant_total_euros", ascending=False, inplace=True)
diff_dpi_dc.head(20)

diff_dpi_dc["montant_remuneration_declarant_total_euros"].sum()

diff_dpi_dc["montant_remuneration_organisme_total_euros"].sum()

(diff_dpi_ts.groupby(["TS_groupe"], as_index=False)
 .agg({"montant_remuneration_organisme_total_euros": np.sum})
 .sort_values(by="montant_remuneration_organisme_total_euros", ascending=False)).head(1000)

(diff_dpi_ts.groupby(["TS_groupe"], as_index=False)
 .agg({"montant_remuneration_declarant_total_euros": np.sum})
 .sort_values(by="montant_remuneration_declarant_total_euros", ascending=False)).head(1000)

diff_dpi_ts[diff_dpi_ts["TS_groupe"]=="Zimmer Biomet"].head(1000)

len(pd.unique(diff_dpi_ts["numero_dpi"]))

all_dpis = pd.read_csv(os.path.join(f.RAW_DATA_DIR, f.DPI_FILE), sep=";", skiprows=1, header=0, names=c.RAW_HEADER)

# # Groupby pour les rémunérations déclarant

(diff_dpi_ts.groupby(["TS_groupe", "nom_prenom_declarant", "identifiant", "sujet"], as_index=False)
 .agg({"montant_remuneration_declarant_total_euros": np.sum})
 .sort_values(by="montant_remuneration_declarant_total_euros", ascending=False)).reset_index(drop=True).head(1000)

# # Groupby pour les rémunérations organisme

# +
gb_rem_org = (diff_dpi_ts.groupby(["TS_groupe", "organisme_beneficiaire", "sujet", "nom_prenom_declarant", "url_declaration_nom_prenom",
                     "url_declaration_rpps", "numero_dpi"], as_index=False)
 .agg({"montant_remuneration_organisme_total_euros": np.sum})
 .sort_values(by="montant_remuneration_organisme_total_euros", ascending=False))

gb_rem_org.head(12)
# -

diff_dpi_ts[diff_dpi_ts["numero_dpi"].isin(
    gb_rem_org[gb_rem_org["montant_remuneration_organisme_total_euros"]>10000]["numero_dpi"])].head(100)

len(diff_dpi_ts)

len(diff_dpi_ts.drop_duplicates())

0.76*12484*0.37


