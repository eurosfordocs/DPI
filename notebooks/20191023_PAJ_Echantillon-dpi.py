# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os
os.chdir('..')

# # Import & Read

import pandas as pd

pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 200

from src.constants.files import *
from src.utils import *

dpi_file = DPI_FILE
# dpi_file = DPI_NO_NAME_SAMPLE

raw_dpi = os.path.join(RAW_DATA_DIR, dpi_file)
interim_dpi = os.path.join(INTERIM_DATA_DIR, dpi_file)


# # Traitement

# +
def concat_str_columns_from_df(df: pd.DataFrame, columns: List[str], sep: str = ' ') -> pd.Series:
    result_series = df[columns[0]]
    for column_name in columns[1:]:
        result_series = concat_two_str_series(result_series, df[column_name], sep=sep)
    return result_series


def concat_two_str_series(str_series_1: pd.Series, str_series_2: pd.Series, sep: str):
    return (str_series_1.fillna('')
            .str.cat(str_series_2.fillna('').values, sep=sep)
            .apply(rchop, args=(sep,))
            .str.strip()
            )


def rchop(s, ending):
    if s.endswith(ending):
        return s[:-len(ending)]
    return s


# -

df = pd.read_csv(interim_dpi, sep=';', dtype="str")
df.head(3)

# +
map_date = {d: pd.to_datetime(d, dayfirst=True) for d in set(df.date_soumission_dpi.unique())}

df.date_soumission_dpi = df.date_soumission_dpi.map(map_date)
# -

df['adresse_employeur'] = concat_str_columns_from_df(
    df,
    ['adresse_employeur', 'code_postal_employeur', 'ville_employeur', 'pays_employeur'])

df = df.sort_values(["date_soumission_dpi", "numero_dpi", "TS_groupe", "rubrique_lien",  "date_debut", "date_fin"], 
                    ascending=[False, False, False, True, True, True],
                    na_position='first')

columns = [
    'url_declaration_nom_prenom',
    'url_declaration_rpps',
    'url_annuaire_rpps',
    'numero_dpi',
    'nom_prenom_declarant',
    'identifiant',
    'rubrique_lien',
    'adresse_employeur',
    'type_activite',  # r1

    'TS_filiale',
    'TS_groupe',
    
    'date_debut',
    'date_fin',

    'organisme_financeur',
    'fonction_occupee',
    'sujet',
    'type_remuneration',
    'montant_remuneration_declarant',
    'montant_remuneration_organisme',
    'complement_montant_organisme',
    'type_etude',
    'organisme_beneficiaire',
    'is_interessement',
    'is_frais_pris_en_charge',

    'index',
]

# +
numero_dpi_with_rubrique_2 = set(df[df.rubrique_lien.str.startswith("§ 2")].numero_dpi)
numero_dpi_with_at_least_1_TS_groupe = set(df[df.TS_groupe.notna()].numero_dpi)
last_numero_dpi_du_nom_prenom = set(
    df[["numero_dpi", "nom_prenom_declarant"]].drop_duplicates("nom_prenom_declarant", keep="first").numero_dpi)
mask_numero_dpi = df.numero_dpi.isin(
    last_numero_dpi_du_nom_prenom & numero_dpi_with_rubrique_2 & numero_dpi_with_at_least_1_TS_groupe)

mask_rpps = df.identifiant.notna()
mask_rubrique_12 = ((df.rubrique_lien == "§ 1. Activité Principale") |
                    (df.rubrique_lien.str.startswith("§ 2"))
                    )


# -

sdf = df.loc[mask_numero_dpi & mask_rubrique_12 & mask_rpps, columns][:1000]

sdf.head(30)

sdf.to_csv("data/interim/sample_dpi_with_rpps_groupe_head1000.csv", index=False)



# ## Traitement activité principale

df.date_soumission_dpi = df.date_soumission_dpi.map(pd.to_datetime)

df.fonction_occupee.str.lower().nunique()

# +
df1 = df[df.rubrique_lien.str.startswith("§ 1")].fillna("")

df1['adresse_employeur'] = concat_str_columns_from_df(
    df1,
    ['adresse_employeur', 'code_postal_employeur', 'ville_employeur', 'pays_employeur'])

df1 = df1[['numero_dpi', 'date_soumission_dpi',
           'nom_prenom_declarant', 'identifiant',
           # 'institution', 'instance', 'fonction_declarant', 'qualite',
           'statut_declaration',
           'type_activite', 'fonction_occupee', 'adresse_employeur', ]]
df1 = df1.sort_values(["date_soumission_dpi"], ascending=False)
df1 = df1.drop_duplicates(
    subset=['numero_dpi', 'nom_prenom_declarant', 'identifiant', 'type_activite', 'fonction_occupee'])
# -

df1.shape

df1.sort_values(["nom_prenom_declarant", "date_soumission_dpi"])

df23 = df[df.rubrique_lien.str.startswith("§ 2") | df.rubrique_lien.str.startswith("§ 3")]

df.columns

df.rubrique_lien.value_counts().sort_index()

mask = df.rubrique_lien.str.startswith("§ 2") | df.rubrique_lien.str.startswith("§ 3")
df.rubrique_lien[mask].value_counts()

df[df.rubrique_lien.str.startswith("§ 3")]

df[mask]

# + {"jupyter": {"outputs_hidden": true}}
raw_data_path = "data/raw/"

sign_cols = pd.read_csv(raw_data_path + "signification_colonnes.csv", sep=",").drop(
    ["Page manuel", "Nb lignes"], axis=1)
sign_cols.head(1)

# + {"jupyter": {"outputs_hidden": true}}
sign_cols_dict = {}

for index, row in sign_cols.iterrows():
    detail_liens_list = [row["detail_lien_{}".format(str(x))] for x in range(1, 11)
                         if row["detail_lien_{}".format(str(x))] not in ["VIDE", "date_debut_fin"]]
    for montant_col in ["montant_remuneration_organisme", "montant_remuneration_declarant"]:
        if montant_col in detail_liens_list:
            detail_liens_list += [montant_col + "_" + suf for suf in ["valeur", "devise", "frequence"]]
    if "organisme_financeur_montant_pourcentage" in detail_liens_list:
        detail_liens_list += ["organisme_financeur", "montant_activites_financees", "pourcentage_activites_financees"]

    detail_liens_list += ["date_debut", "date_fin", "numero_dpi", "nom_prenom_declarant"]
    sign_cols_dict[row["rubrique_lien"]] = detail_liens_list

sign_cols_dict
# -

sign_cols_dict

df.loc[df.rubrique_lien.str.startswith("§ 1"), [
    'nom_prenom_declarant',
    'type_activite',
    'fonction_occupee',

    'numero_dpi',
]]

# ## analyse statut_declaration
#

declarant_historise = set(df.loc[df.statut_declaration == 'Historisée', 'nom_prenom_declarant'])
declarant_soumise = set(df.loc[df.statut_declaration == 'Soumise', 'nom_prenom_declarant'])

len(declarant_historise)

len(declarant_soumise)

len(declarant_historise - declarant_soumise)

# ## Analyse fonction_declarant et qualite

df1.fonction_declarant.value_counts()

df1.qualite.value_counts()

(df1.fonction_declarant + " / " + df1.qualite).value_counts()
