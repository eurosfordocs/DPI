# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import sys
sys.path.append("..")

decl_conc = pd.read_csv("../data/interim/declarations_concatenees.csv.gz", sep=";")
decl_conc.head()

len(decl_conc)

print(len(decl_conc[pd.isnull(decl_conc["identifiant"]) & (~pd.isnull(decl_conc["type_identifiant"]))]))


