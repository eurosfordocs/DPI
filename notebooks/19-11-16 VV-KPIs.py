# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import sys
sys.path.append("..")

from src.preprocess.create_interim_dpi.identifiant_professionnel import preprocess_annuaire_pro

annuaire = preprocess_annuaire_pro(drop_non_rpps=True, drop_homonyms=False)
print(len(annuaire))

annuaire["nom_prenom"] = annuaire["nom_declarant"] + " " + annuaire["prenom_declarant"]

np_val_counts = pd.DataFrame(annuaire["nom_prenom"].value_counts()).reset_index()
np_val_counts.columns = ["nom_prenom", "nombre"]
np_val_counts.head()
# -

len(np_val_counts[np_val_counts["nombre"]>1]) / len(np_val_counts)


