# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import sys
sys.path.append("..")
import seaborn as sns

# %matplotlib inline
import matplotlib.pyplot as plt
pd.set_option("max_columns", 100)
pd.set_option("max_rows", 500)
pd.set_option('max_colwidth', 400)

import src.constants.columns as c
import src.constants.files as f

import os

from src import utils

from src.kpi.kpi_utils import (compute_ratio, concat_healthcare_dpi, filter_dpi_on_people_match,
                               filter_dpi_on_company_match, filter_dpi_on_people_company_match)

# +
diff_dpi_ts = pd.read_csv(os.path.join(f.RESULT_DATA_DIR, "{}_{}.csv".format(f.DIFF_DPI_TS, f.FULL)), sep=";")
dpi_rubr_2_rem = pd.read_csv(os.path.join(f.INTERIM_DATA_DIR, "20190903001500_export_declarations_publiees_rubr_2_rem.csv"),
                            sep=";", low_memory=False)
ts = pd.read_csv(os.path.join(f.INTERIM_DATA_DIR, "declarations_concatenees.csv.gz"), sep=";")
df_healthcare = concat_healthcare_dpi(dpi_rubr_2_rem)

dpi_matchables = filter_dpi_on_people_company_match(df_healthcare)


# -

def format_pct(ratio):
    return str(round(100*ratio, 1)) + "%"


def values_from_dfs(dfs, value_type):
    amounts = []
    for df in dfs:
        if value_type == "sum":
            sum_amounts_rubr_2_declarant = round(df[c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS].sum() / 10 ** 6, 1)
            amounts.append(sum_amounts_rubr_2_declarant)
        elif value_type == "len":
            amounts.append(len(df))
    return amounts


def plot_barplot(labels, dfs, value_type="sum"):
    amounts = values_from_dfs(dfs, value_type)
    plt.figure(1, figsize=(20, 8))
    plt.rcParams.update({'font.size': 22})
    if value_type == "sum":
        plt.ylabel("Montant rémunération déclarant (M€)")
    elif value_type == "len":
        plt.ylabel("Nombre de lignes de déclarations")
    plt.title("valeurs {}, ratios successifs: {}".format(
        ", ".join([str(x) for x in amounts]),
        ", ".join([format_pct(amounts[i]/amounts[i-1]) for i in range(1, len(amounts))])))
    plt.bar(labels, amounts)


labels = ["dpi candidates", "dpi matchables", "dpi matchables non trouvées dans TS"]
dfs = [df_healthcare, dpi_matchables, diff_dpi_ts]
plot_barplot(labels, dfs)

# +
labels = ["dpi candidates", "dpi matchables", "dpi matchables non trouvées dans TS"]
dfs = [df_healthcare, dpi_matchables, diff_dpi_ts]

plot_barplot(labels, dfs, value_type="len")

# +
import math

def bubble_plot(labels, dfs, size_ratio, output_name=None, value_type="sum"):
    amounts = values_from_dfs(dfs, value_type)
    
    plt.rcParams.update({'font.size': 18})
    plt.figure(1, figsize=(20, 20))
    for i in range(len(amounts)):
        plt.scatter(i, 0, s=amounts[i]*size_ratio, label=labels[i])
    lgnd = plt.legend()
    for handle in lgnd.legendHandles:
        handle.set_sizes([60.0])
    plt.axis('off')
    if value_type == "sum":
        plt.title("Montant rémunération déclarant (M€)")
    elif value_type == "len":
        plt.title("Nombre de lignes de déclarations")
    if output_name is not None:
        plt.savefig(os.path.join(f.RESULT_DATA_DIR, output_name))
    plt.show()


# -

output_name = "ratios de montants en jeu.png"
labels = ["dpi candidates", "dpi matchables", "dpi matchables non trouvées dans TS"]
dfs = [df_healthcare, dpi_matchables, diff_dpi_ts]
bubble_plot(labels, dfs, 1000, output_name)

output_name = "ratios de nombre de lignes en jeu.png"
labels = ["dpi candidates", "dpi matchables", "dpi matchables non trouvées dans TS"]
dfs = [df_healthcare, dpi_matchables, diff_dpi_ts]
bubble_plot(labels, dfs, 10, output_name, value_type="len")



