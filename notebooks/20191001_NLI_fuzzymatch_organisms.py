# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %matplotlib inline

# +
import os

os.chdir('..')

from IPython.core.interactiveshell import InteractiveShell

InteractiveShell.ast_node_interactivity = "all"
# -

# # Import & Read

import pandas as pd

pd.options.display.max_columns = 100

# +
from src.constants.files import *

from src.utils import *
# -

# # Import csv

# ## DPI to TS

df = pd.read_csv(DPI_TO_TS_ORGANISME_PATH, sep=',')
df.shape
df.head()

# Get not matched organisms
df_null = df[df['TS_filiale'].isnull() & df['TS_groupe'].isnull()]
df_null.shape
df_null.head()

# ## Entreprises TS List

raw_ts_entreprises = os.path.join(RAW_DATA_DIR, 'entreprise.csv')

df_ts_entreprises = pd.read_csv(raw_ts_entreprises, sep=',')
df_ts_entreprises.loc[:, 'entreprise_émmetrice_normalized'] = df_ts_entreprises['entreprise_émmetrice'].apply(
    lambda x: x if pd.isna(x) else normalize_string(x))
df_ts_entreprises = df_ts_entreprises.drop_duplicates(subset=['entreprise_émmetrice_normalized'])
df_ts_entreprises.shape
df_ts_entreprises.head()

# # Exact match

df_with_exact_match = pd.merge(
    df_null[['organisme_emetteur']],
    df_ts_entreprises[['entreprise_émmetrice', 'entreprise_émmetrice_normalized']],
    left_on='organisme_emetteur',
    right_on='entreprise_émmetrice_normalized',
    how='left'
)

# df_exact = df_exact[~df_exact['entreprise_émmetrice'].isnull()]
df_with_exact_match['method'] = 'exact_match'
df_with_exact_match.info()
df_with_exact_match.head()

# Get exact matched rows
df_exact_match = df_with_exact_match[~df_with_exact_match['entreprise_émmetrice'].isnull()]
df_exact_match = df_exact_match.drop(['entreprise_émmetrice_normalized'], axis=1)
df_exact_match.shape
df_exact_match.head()

# # Fuzzy match on remaining null

# Get remaining nulls
df_null_after_exact = df_with_exact_match[df_with_exact_match['entreprise_émmetrice'].isnull()].reset_index(drop=False)
df_null_after_exact.shape
df_null_after_exact.head()

from fuzzywuzzy import fuzz, process
from tqdm.autonotebook import tqdm

from string import ascii_lowercase
# dictionary to speed up search
dic_ts_entreprises = {c: {e_norm: e_org for (e_norm, e_org) in zip(df_ts_entreprises['entreprise_émmetrice_normalized'],
                                                                   df_ts_entreprises['entreprise_émmetrice']) if
                          e_norm[0] == c} for c in ascii_lowercase + '0123456789'}


# +
entreprises_matches = {}

for index, dpi_org in tqdm(
        enumerate(df_null_after_exact['organisme_emetteur'])):  # for some reason normalize string is not good enough?
    try:
        entreprises_letter_dic = dic_ts_entreprises[dpi_org[0]]
        match = process.extractOne(
            dpi_org,
            list(entreprises_letter_dic.keys()),
            score_cutoff=90,
            scorer=fuzz.QRatio
        )
        if match is not None:
            entreprises_matches[dpi_org] = entreprises_letter_dic[match[0]]

    except Exception as e:
        print('Error: ', e)
        print(index, dpi_org)
        print('----')
        continue
# -

entreprises_matches

df_fuzzy_match = pd.DataFrame({'organisme_emetteur': list(entreprises_matches.keys()),
                               'entreprise_émmetrice': list(entreprises_matches.values())})
df_fuzzy_match['method'] = 'fuzzy_match'
df_fuzzy_match.shape
df_fuzzy_match.head()

# # Results To CSV

df_final = pd.concat([df_exact_match, df_fuzzy_match], sort=False)
df_final.shape
df_final.to_csv('exact_fuzzy_match_dpi_entreprises.csv', index=False)
