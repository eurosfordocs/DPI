# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

def add_column_rpps_homonym(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add RPPS column from annuaire. At the moment, only add unique fullnames matching
    """
    logging.info(
        "- Ajout d'une colonne identifiant à partir de l'annuaire des professionnels de santé, incluant les homonymes")
    df_dpi_matched = df[~df[c.IDENTIFIANT_PROFESSIONNEL].isnull()]
    df_dpi_not_matched = df[df[c.IDENTIFIANT_PROFESSIONNEL].isnull()]

    df_annuaire_normalized = preprocess_annuaire_pro_normalize_profession(drop_non_rpps=False, keep_identifiant=True)
    df_dpi_simple_normalized = preprocess_dpi(df_dpi_not_matched)
    df_dpi_simple_with_id = merge_dpi_id(df_dpi=df_dpi_simple_normalized, df_annuaire=df_annuaire_normalized)
    df_dpi_with_id = pd.merge(
        df_dpi_not_matched.drop([c.IDENTIFIANT_PROFESSIONNEL], axis=1),
        df_dpi_simple_with_id[[c.NUMERO_DPI, c.IDENTIFIANT_PROFESSIONNEL]].drop_duplicates(),
        on=c.NUMERO_DPI,
        how='left'
    )
    return pd.concat([df_dpi_matched, df_dpi_with_id], sort=False)


def extract_information_column_organisme_montant_pourcentage(df: pd.DataFrame) -> pd.DataFrame:
    """ Extract the information of the column organisme_montant_pourcentage in the in the different appropriated columns
    """

    logging.info("- Extraction informations organisme, montant, et pourcentage.")
    # Mask computation of the specific link
    mask = df.rubrique_lien == '§ 3. Activités financées par un organisme à but lucratif'

    # Extraction pourcentage with regex
    df = df.join(df[c.ORGANISME_MONTANT_POURCENTAGE]
                 .str.extract(r'(?P<{}>\d+\.?,?\d*)\s*%'.format(c.POURCENTAGE_ACTIVITES_FINANCEES))
                 )

    df[c.POURCENTAGE_ACTIVITES_FINANCEES] = df[c.POURCENTAGE_ACTIVITES_FINANCEES].str.replace(',', '.')

    # Extraction montant with regex after elimination of pourcentage
    split_montant = df[c.ORGANISME_MONTANT_POURCENTAGE].str.split(r'\d+\.?,?\d*\s*%', expand=True)[0]
    df = df.join(
        split_montant.str.extract(r'\s(?P<{}>\d*\.?\s?\d*\.?\s?\d+\.?,?\d*)'.format(c.MONTANT_ACTIVITES_FINANCEES)))

    df[c.MONTANT_ACTIVITES_FINANCEES] = df[c.MONTANT_ACTIVITES_FINANCEES] \
        .map(lambda s: ''.join(s.split()) if isinstance(s, str) else s)

    # Extraction organisme after elimination of pourcentage and montant
    split_organisme = \
        df[mask][c.ORGANISME_MONTANT_POURCENTAGE].str.split(r'\s\d*\.?\s?\d*\.?\s?\d+\.?,?\d*', expand=True)[0]
    df.loc[mask, c.ORGANISME] = pd.DataFrame({c.ORGANISME: split_organisme})

    return df



# +
# Identifiant professionel homonym

import pandas as pd

from src.constants import columns as c
from src.preprocess.create_interim_dpi.identifiant_professionnel import preprocess_annuaire_pro
from src.utils import normalize_string

normalize_profession_annuaire_dic = {
    'Audioprothésiste': 'autre',
    'Opticien-Lunetier': 'autre',
    'Infirmier': 'infirmier',
    'Ergothérapeute': 'autre',
    'Orthophoniste': 'autre',
    'Technicien de laboratoire médical': 'autre',
    'Diététicien': 'medecin',
    'Podo-Orthésiste': 'medecin',
    'Psychomotricien': 'autre',
    'Orthoprothésiste': 'autre',
    'Orthopédiste-Orthésiste': 'medecin',
    'Orthoptiste': 'medecin',
    'Sage-Femme': 'sage femme',
    'Médecin': 'medecin',
    'Pharmacien': 'pharmacien',
    'Chirurgien-Dentiste': 'medecin',
    'Masseur-Kinésithérapeute': 'kine',
    'Pédicure-Podologue': 'medecin',
    'Epithésiste': 'autre',
    'Oculariste': 'autre',
}


def normalize_fonction_dpi(fonction: str):
    if pd.isna(fonction):
        return 'autre'
    if 'pharmacien' in fonction:
        return 'pharmacien'
    if 'sage' in fonction:
        return 'sage femme'
    if 'technicien' in fonction:
        return 'autre'
    if 'kine' in fonction:
        return 'kine'
    if 'masseur' in fonction:
        return 'kine'
    if 'infirmier' in fonction:
        return 'infirmier'
    if 'professeur' in fonction or 'maitre' in fonction or 'enseignant' in fonction:
        return 'medecin'
    if 'directeur' in fonction or 'directrice' in fonction:
        return 'medecin'
    if 'responsable' in fonction:
        return 'medecin'
    if 'president' in fonction:
        return 'medecin'
    if 'praticien' in fonction:
        return 'medecin'
    if 'chercheur' in fonction:
        return 'medecin'
    if 'medecin' in fonction:
        return 'medecin'
    if 'chirurg' in fonction:
        return 'medecin'
    if 'radio' in fonction:
        return 'medecin'
    if 'psychiatre' in fonction:
        return 'medecin'
    if fonction in ['puph', 'ph', 'pu', 'pu-ph', 'mcu-ph',
                    'mcu'] or 'ph ' in fonction or 'pu-' in fonction or 'pu ' in fonction:
        return 'medecin'
    if 'chef' in fonction:
        return 'medecin'
    return 'autre'


def preprocess_annuaire_pro_normalize_profession(drop_non_rpps=False, keep_identifiant=True) -> pd.DataFrame:
    df_annuaire = preprocess_annuaire_pro(drop_non_rpps, keep_identifiant)
    df_annuaire[c.PROFESSION_NORM] = df_annuaire[c.PROFESSION].replace(normalize_profession_annuaire_dic)
    df_annuaire = df_annuaire.drop_duplicates(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, c.PROFESSION_NORM],
                                              keep=False)
    return df_annuaire


def preprocess_dpi(df: pd.DataFrame) -> pd.DataFrame:
    df_simple = df.copy()
    df_simple.loc[:, c.FONCTION_OCCUPEE] = df_simple[c.FONCTION_OCCUPEE].apply(normalize_string)
    df_simple = df_simple.drop_duplicates(
        subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, c.FONCTION_OCCUPEE, c.NUMERO_DPI])
    df_simple = df_simple[[c.NOM_DECLARANT, c.PRENOM_DECLARANT, c.FONCTION_OCCUPEE, c.NUMERO_DPI]]
    df_simple[c.FONCTION_OCCUPEE_NORM] = df_simple[c.FONCTION_OCCUPEE].apply(normalize_fonction_dpi)
    df_simple = df_simple.dropna(subset=[c.FONCTION_OCCUPEE_NORM])
    return df_simple


def merge_dpi_id(df_dpi: pd.DataFrame, df_annuaire: pd.DataFrame) -> pd.DataFrame:
    df_with_id = pd.merge(
        df_dpi,
        df_annuaire,
        left_on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, c.FONCTION_OCCUPEE_NORM],
        right_on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT, c.PROFESSION_NORM],
        how='inner',
    ).drop_duplicates(subset=[c.NUMERO_DPI, c.IDENTIFIANT_PROFESSIONNEL])  # Drop duplicates
    # Drop rows with duplicate numero_dpi but different identifiant
    return df_with_id.groupby(c.NUMERO_DPI).filter(
        lambda g: len(g[c.IDENTIFIANT_PROFESSIONNEL].unique()) == 1).drop_duplicates(
        subset=[c.NUMERO_DPI, c.IDENTIFIANT_PROFESSIONNEL])

