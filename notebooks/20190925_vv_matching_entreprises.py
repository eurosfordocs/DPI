# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
import sys
sys.path.append("../src")
import utils
# %matplotlib inline
pd.set_option("max_columns", 100)
pd.set_option('max_colwidth', 400)

# %%
data_path = "../data/interim/"
raw_data = "../data/raw/"
etalab_path = "../../exports-etalab/"

# %%
dpi = pd.read_csv(data_path + "dpi_no_name.csv", sep=";", low_memory=False)
dpi.head(1)

# %%
ts_rem = pd.read_csv(etalab_path + "declaration_remuneration_2019_09_11_04_00.csv", sep=";", low_memory=False)
ts_rem.head(1)

# %%
dpi_orgs = dpi[["organisme_emetteur"]].drop_duplicates()
ts_orgs = ts_rem[["denomination_sociale"]].drop_duplicates()

print("Nombre d’organismes dpi: {}".format(len(dpi_orgs)))
print("Nombre d’organismes ts: {}".format(len(ts_orgs)))

# %% [markdown]
# # Matching avec l’inclusion des mots dpi dans ts

# %%
import nltk
from collections import Counter
nltk.download("stopwords")

# Word normalization and split into list
def org_to_word_list(org):
    norm_org = utils.normalize_string(str(org))
    word_list = list(set([word for word in norm_org.split(" ")
                 if word not in nltk.corpus.stopwords.words('french')
                 and word not in nltk.corpus.stopwords.words('english')
                 and "-" not in word
                 and len(word) > 2]))
    return word_list

ts_orgs["word_list"] = ts_orgs["denomination_sociale"].apply(org_to_word_list)
dpi_orgs["word_list"] = dpi_orgs["organisme_emetteur"].apply(org_to_word_list)

# %% [markdown]
# # Filtrage des mots les plus communs

# %% [markdown]
# Source mots anglais: https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt
# Source mots francais: http://www.pallier.org/extra/liste.de.mots.francais.frgut.txt

# %%
nb_most_common_words = 100

def get_x_most_common_words(df, col, nb_words):
    all_df_words = df[col].sum()
    counter = Counter()

    for word in all_df_words:
        counter.update([word])

    return [x[0] for x in counter.most_common(nb_words)]

with open('../data/raw/liste.de.mots.francais.frgut.txt', 'r') as f:
    french_words = f.readlines()
french_words_clean = [utils.normalize_string(word.replace("\n", "")) for word in french_words]

with open('../data/raw/words_alpha.txt', 'r') as f:
    english_words = f.readlines()
english_words_clean = [utils.normalize_string(word.replace("\n", "")) for word in english_words]

real_words = list(set(french_words_clean + english_words_clean))

most_common_words_dpi = get_x_most_common_words(dpi_orgs, "word_list", nb_most_common_words)
most_common_words_ts = get_x_most_common_words(ts_orgs, "word_list", nb_most_common_words)

most_common_real_words = sorted(list(set(
    [word for word in most_common_words_dpi + most_common_words_ts if word in real_words])))

print(most_common_real_words)

# %%
authorized_words = ["amicus", "anses", "ars", "balt", "bayer", "bristol", "edwards", "roche"]

forbidden_words = [word for word in most_common_real_words if word not in authorized_words]

# %%
dpi_orgs["clean_word_list"] = dpi_orgs["word_list"].apply(
    lambda x: [word for word in x if word not in forbidden_words])
ts_orgs["clean_word_list"] = ts_orgs["word_list"].apply(
    lambda x: [word for word in x if word not in forbidden_words])

dpi_orgs.head(3)

# %% [markdown]
# # Matching

# %%
# Il faut que la liste de mots de l’organisme dpi et celle de ts aient au moins nb_required_matches mots en communs
# pour faire l’association

nb_required_matches = 2

matching_dict_list = []
for clean_dpi_word_list, dpi_word_list, dpi_org in zip(
    dpi_orgs["clean_word_list"], dpi_orgs["word_list"], dpi_orgs["organisme_emetteur"]):
    for clean_ts_word_list, ts_word_list in zip(ts_orgs["clean_word_list"], ts_orgs["word_list"]):
        if len(set(clean_dpi_word_list).intersection(set(clean_ts_word_list))) >= nb_required_matches:
            matching_dict_list.append({
                "org_ts": " ".join(ts_word_list), "org_dpi": " ".join(dpi_word_list),
                "clean_org_ts": " ".join(clean_ts_word_list), "clean_org_dpi": " ".join(clean_dpi_word_list),
                "dpi_organisme_emetteur": dpi_org})

# %%
matching_df = pd.DataFrame.from_records(matching_dict_list).drop_duplicates()

# %%
matching_df.head(3)

# %%
nb_organismes_ts_match = len(pd.unique(matching_df["clean_org_ts"]))
nb_organismes_dpi_match = len(pd.unique(matching_df["clean_org_dpi"]))

nb_organismes_uniques_dpi = len(pd.unique(dpi_orgs["clean_word_list"].apply(lambda x: " ".join(x))))
nb_organismes_uniques_ts = len(pd.unique(ts_orgs["clean_word_list"].apply(lambda x: " ".join(x))))

print("Le matching par une jointure par match de {} mots touche {}% des organismes dpi "
      " et {}% des organismes ts "
      "({} organismes dpi matchés à {} organismes ts).".format(
    nb_required_matches,
    round(100*nb_organismes_dpi_match / nb_organismes_uniques_dpi, 1),
    round(100*nb_organismes_ts_match / nb_organismes_uniques_ts, 1),
    nb_organismes_dpi_match, nb_organismes_ts_match))

# %%
nb_organismes_uniques_dpi


# %%
len(matching_df)

# %%
matching_df.to_csv(data_path + "organism_matching.csv", sep=";", index=False)

# %%
nb_dpi_avec_organisme_match = len(pd.merge(dpi, matching_df, left_on="organisme_emetteur",
                                          right_on="dpi_organisme_emetteur", how="inner"))

# %%
print("{}% des déclarations dpi sont matchées.".format(round(100*nb_dpi_avec_organisme_match / len(dpi),1)))

# %%
nb_dpi_avec_organisme_match

# %%

