# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# %matplotlib inline

import os
os.chdir('..')

import pandas as pd
pd.options.display.max_columns=50


data_path = "data/raw/20190903001500_export_declarations_publiees.csv"

header = [
    'institution',
    'instance',
    'date_debut_instance',
    'date_fin_instance',
    'nom_declarant',
    'prenom_declarant',
    'fonction_declarant',
    'qualite',
    'statut_declaration',
    'date_debut_mandat',
    'date_fin_mandat',
    'numero_dpi',
    'date_soumission_dpi',
    'rubrique_lien',
    'detail_lien_1',
    'detail_lien_2',
    'detail_lien_3',
    'detail_lien_4',
    'detail_lien_5',
    'detail_lien_6',
    'detail_lien_7',
    'detail_lien_8',
    'detail_lien_9',
    'detail_lien_10',
    'detail_lien_11'
]

df = pd.read_csv(data_path, sep=';', skiprows=1, header=0, names=header)
df.head(3)

df.nom_declarant = 'NOM'
df.prenom_declarant = 'Prenom'

df.to_csv("data/raw/dpi_no_name.csv", sep=';', index=False)

df_list = []
for rubrique in df.rubrique_lien.unique():
    mask = df.rubrique_lien == rubrique
    df_list.append(df[mask].sample(10, random_state=42))
df_sample = pd.concat(df_list)

df_sample.to_csv("data/raw/dpi_no_name_sample.csv", sep=';', index=False)

# + {"jupyter": {"outputs_hidden": true}}

