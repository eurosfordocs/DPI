# Installation du projet

Note : Cette procédure devrait fonctionner sur Linux et Mac. Il faut la tester et la compléter pour Windows.

## Dépendances

Le projet utilise `Python3` et des librairies listées dans le fichier [requirements](requirements.txt). 

Nous recommendons d'installer ces dépendances dans un environnement virtuel.

Tester l'installation de `Python3` et `virtualenv` avec ces commandes

    python3 --version
    pip3 --version
    virtualenv --version

Installer `virtualenv` si besoin avec la commande :  `pip3 install virtualenv`

## Installation 

Cloner le projet en local

    git clone https://gitlab.com/eurosfordocs/dpi.git
    cd dpi

Créer un environnement virtuel, l'activer et installer les dépendances Python

    virtualenv venv --python=python3
    source venv/bin/activate 
    pip3 install -r requirements.txt

Tester l'installation

    python -m pytest tests
