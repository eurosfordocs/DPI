import logging
from os.path import join

import pandas as pd

from src.constants import columns as c
from src.constants.files import INTERIM_DATA_DIR, RESULT_DATA_DIR, DIFF_DPI_TS, DPI_RUBR_2
from src.constants.files import DPI_FILE, TS_CONCATENATED


def get_dpi_not_in_ts() -> None:
    # TODO: docstring
    logging.info("Recherche des DPI absentes de TS")

    logging.info("- Chargement des dataframes DPI rubrique 2 avec paiement et déclarations TS concaténées.")
    dpi_rubr_2_rem_file_path = join(INTERIM_DATA_DIR, DPI_RUBR_2)
    dpi = pd.read_csv(dpi_rubr_2_rem_file_path, sep=";", low_memory=False)
    concat_ts = pd.read_csv(join(INTERIM_DATA_DIR, TS_CONCATENATED), sep=";")

    diff_dpi_ts = dpi_ts_matching(dpi, concat_ts)
    logging.info("- {} DPI ne sont pas retrouvées dans TS".format(len(diff_dpi_ts)))

    diff_dpi_ts_file_path = join(RESULT_DATA_DIR, DIFF_DPI_TS)
    logging.info(f"- Écriture des DPI non trouvées dans TS dans le fichier {DIFF_DPI_TS}")
    diff_dpi_ts.to_csv(diff_dpi_ts_file_path, sep=';', index=False)


def dpi_ts_matching(df_dpi: pd.DataFrame, df_ts: pd.DataFrame) -> pd.DataFrame:
    """
    Extraction des déclarations dpi liées à une entreprise TS pour lesquelles l’identifiant RPPS n’est pas lié à
    l’entreprise dans TS.

    :param df_dpi: Fichier dpi normalisé.
    :param df_ts: Concaténation des entreprises TS et leurs identifiants associés.
    :return: Dataframe des déclarations dpi non présentes dans TS.
    """
    link_columns = [c.TS_GROUPE, c.TS_IDENTIFIANT]

    mask_dpi_with_ids = ~pd.isnull(df_dpi[c.TS_GROUPE]) & ~pd.isnull(df_dpi[c.TS_IDENTIFIANT])
    df_dpi = df_dpi[mask_dpi_with_ids].copy()
    df_dpi[c.TS_IDENTIFIANT] = df_dpi[c.TS_IDENTIFIANT].map(lambda x: str(int(x)))

    df_ts_ids = (df_ts[[c.TS_ORGANISME, c.TS_IDENTIFIANT]]
                 .rename(columns={c.TS_ORGANISME: c.TS_GROUPE})
                 .drop_duplicates()
                 )

    ts_links = set(df_ts_ids.itertuples(index=False))
    logging.info(" - {} liens distincts TS".format(len(ts_links)))

    dpi_links = set(df_dpi[link_columns].itertuples(index=False))
    logging.info(" - {} liens distincts DPI".format(len(dpi_links)))

    diff_dpi_ts_links = dpi_links.difference(ts_links)
    logging.info(" - {} liens distincts DPI absents de TS".format(len(diff_dpi_ts_links)))

    mask_dpi_links_in_ts = df_dpi[link_columns].apply(tuple, axis=1).isin(ts_links)
    df_dpi_links_not_in_ts = df_dpi[~mask_dpi_links_in_ts]
    logging.info(" - {} lignes DPI dont le lien est absent dans TS".format(len(df_dpi_links_not_in_ts)))

    return df_dpi_links_not_in_ts


if __name__ == '__main__':
    get_dpi_not_in_ts(DPI_FILE, True)
