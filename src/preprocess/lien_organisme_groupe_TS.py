import pandas as pd
import logging

from src.constants import columns as c
from src.constants.files import DPI_TO_TS_ORGANISME_PATH, COMPANY_GROUP_PATH, LIEN_ORGANISME_GROUPE_TS


def preprocess_lien_organisme_groupe_ts():
    # TODO: docstring
    # TODO: refactoring de la fonction
    logging.info("Preprocessing link between organisms and TS groups.")

    denomination_sociale = "denomination_sociale"
    groupe = "groupe"

    # Fichier d’association manuelle des organismes financeurs DPI avec les filiales et groupes TS
    # https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/edit#gid=1493365784
    df_organisme = pd.read_csv(DPI_TO_TS_ORGANISME_PATH)[[c.ORGANISME, c.TS_FILIALE, c.TS_GROUPE]]

    # Fichier générique d’association des entreprises aux groupes
    # https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E
    df_company_to_group = pd.read_csv(COMPANY_GROUP_PATH)[[denomination_sociale, groupe]].drop_duplicates()

    # Remplissage du groupe par denomination_sociale quand une filiale n'a pas de groupe
    df_company_to_group[groupe] = df_company_to_group.groupe.fillna(df_company_to_group[denomination_sociale])

    df_organisme = df_organisme.merge(
        df_company_to_group, how="left", left_on=c.TS_FILIALE, right_on=denomination_sociale, validate="many_to_one")

    # Vérification de la cohérence des associations entreprise / groupe faites dans les 2 fichiers
    mask_filiale_and_groupe = df_organisme[c.TS_FILIALE].notna() & df_organisme[c.TS_GROUPE].notna()
    incoherences = (df_organisme.loc[mask_filiale_and_groupe, c.TS_GROUPE]
                    != df_organisme.loc[mask_filiale_and_groupe, groupe])
    if incoherences.any():
        df_incoherence = df_organisme[mask_filiale_and_groupe][incoherences][[c.TS_FILIALE, c.TS_GROUPE, groupe]]
        raise ValueError("Incohérence sur l'association des organismes DPI vers TS entre les filiales et groupes: \n{}"
                         .format(df_incoherence))

    # Remplacement de TS_groupe par le groupe corespondant à TS_filiale
    mask_filiale_remplie = df_organisme[c.TS_FILIALE].notna()
    df_organisme.loc[mask_filiale_remplie, c.TS_GROUPE] = df_organisme.loc[mask_filiale_remplie, groupe]

    df_organisme[[c.ORGANISME, c.TS_FILIALE, c.TS_GROUPE]].to_csv(LIEN_ORGANISME_GROUPE_TS, sep=";", index=False)
