import pandas as pd

from src.constants import columns as c
from src.utils import normalize_string

dic_healthcare = {
    'yes': [
        'praticien',
        'medecin',
        'pu ',
        'ph',
        'sanitaire',
        'sante',
        'chirurgi',
        'cardiolog',
        'generaliste',
        'mcu',
        'mcu-ph',
        'psych',
        'epidemi',
        'pharma',
        'medic',
        'infirm',
        'intern',
        'sage-femme',
        'sage femme',
        'radio',
        'dr ',
        'soin',
        'clini',
        'urgen',
        'ortop',
        'orthop',
        'ophtal',
        'uro',
        'veter',
        'doct',
        'dentis',
        'pediat',
        'masseur',
        'kine',
        'pedic',
        'podolo',
        'nephro',
        'phu',
        'gyneco',
        'anesthe',
        'soign',
        'rhumato',
        'hospita',
        'hopita',
        'ambul',
        'cancer',
        'hemovi',
        'toxico',
        'neuro',
        'pneumo',
        'dieteti',
        'psychi',
        'pueri',
        'dermato',
        'allergo',
        'ansm',
        'vaccin',
    ],
    'maybe': [
        'evaluateur',
        'professeur',
        'enseignant',
        'consultant',
        'etudiant',
        'retraite',
        'directeur',
        'directrice',
        'maitre',
        'unite',
        'ingenieur',
        'responsable',
        'mission',
        'charge',
        'chef',
        'service',
        'charge',
        'benevol',
        'chercheur',
        'secretaire',
        'technique',
        'president',
        'inserm',
        'scientif',
        'cadre',
        'technici',
        'labo',
    ],
}


def get_reversed_dict(value_to_list):
    mapping = dict()
    for value, list_ in value_to_list.items():
        for list_element in list_:
            mapping[list_element] = value
    return mapping


def get_unique_dpi_rubr_1(dpi_full: pd.DataFrame) -> pd.DataFrame:
    """
    Keeps unique dpi number of rubr. 1
    :param dpi_full:
    :return:
    """
    df_dpi = dpi_full.copy()
    df_dpi[c.FONCTION_OCCUPEE_NORM] = df_dpi[c.FONCTION_OCCUPEE].apply(normalize_string)
    df_dpi = df_dpi.dropna(subset=[c.FONCTION_OCCUPEE_NORM])

    df_dpi_activite_principale = df_dpi[df_dpi[c.RUBRIQUE] == c.RUBRIQUE_LIEN_1]

    df_dpi_activite_principale = df_dpi_activite_principale.drop_duplicates(subset=[c.NUMERO_DPI])

    return df_dpi_activite_principale


def categorize_dpi_people(df: pd.DataFrame) -> pd.DataFrame:
    """
    Rajoute la colonne IS_HEALTHCARE au df. On considère les activités principales du df (rubr.1), on les match et on
    complète le df original en se basant sur les numéros dpi
    :param df:
    :return:
    """
    df_activite = get_unique_dpi_rubr_1(df)

    dic_healthcare_reversed = get_reversed_dict(dic_healthcare)

    for index, row in df_activite.iterrows():
        match = [word for word in dic_healthcare_reversed.keys() if word in row[c.FONCTION_OCCUPEE_NORM]]
        if match:
            df_activite.loc[index, c.IS_HEALTHCARE] = dic_healthcare_reversed[match[0]]

    df = pd.merge(df, df_activite[[c.NUMERO_DPI, c.IS_HEALTHCARE]], on=c.NUMERO_DPI, how='left')
    return df
