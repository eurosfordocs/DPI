import logging
from os.path import join

import pandas as pd

from src.constants import columns as c
from src.constants.files import (COLUMNS_SIGNIFICATION_PATH, RAW_DATA_DIR, INTERIM_DATA_DIR, ANNUAIRE_PRO_CLEANED,
                                 LIEN_ORGANISME_GROUPE_TS, DPI_FILE, DPI_RUBR_2)
from src.preprocess.create_interim_dpi.extract_montant import extract_montant
from src.preprocess.create_interim_dpi.url_eurosfordocs import add_eurosfordocs_url
from src.utils import normalize_string
from src.preprocess.create_interim_dpi.categorize_people import categorize_dpi_people


def create_interim_dpi():
    """
    Apply a series of transformations to the raw DPI data to clean it.

    :param dpi_file_name: name of the raw DPI file
    :return: None
    """
    logging.info("Création d'un fichier de DPI intermédiaire")
    raw_dpi = pd.read_csv(join(RAW_DATA_DIR, DPI_FILE), sep=';', skiprows=1, header=0, names=c.RAW_HEADER)

    preprocessed_dpi = preprocess_dpi(raw_dpi)

    dpi_rubr_2_rem_file_path = join(INTERIM_DATA_DIR, DPI_RUBR_2)
    logging.info("- Écriture du dataframe de DPI rubrique 2 à l'emplacement '{}'".format(dpi_rubr_2_rem_file_path))
    preprocessed_dpi.to_csv(dpi_rubr_2_rem_file_path, sep=';', index=False)


def preprocess_dpi(raw_dpi):
    """
    Apply all preprocessing functions to raw DPI data.

    :param raw_dpi: Raw DPI dataframe.
    :return: preprocessed DPI dataframe.
    """
    preprocessed_dpi = (raw_dpi
                        .pipe(extract_common_columns)
                        .pipe(normalize_columns)
                        .pipe(add_column_nom_prenom)
                        .pipe(add_column_rpps)
                        .pipe(add_date_debut_fin)
                        .pipe(categorize_dpi_people)
                        .pipe(filter_out_activity_only_dpi)
                        .pipe(drop_dpi_duplicates)
                        .pipe(keep_last_dpi)
                        .pipe(get_dpi_rubr_2_rem)
                        .pipe(extract_montant)
                        .pipe(add_column_TS_groupe)
                        .pipe(add_eurosfordocs_url)
          )

    output_cols = [c.NUMERO_DPI, c.RUBRIQUE, c.NOM_PRENOM_DECLARANT, c.IDENTIFIANT_PROFESSIONNEL, c.TS_TYPE_IDENTIFIANT,
                   c.TYPE_ACTIVITE, c.IS_HEALTHCARE, c.ADRESSE_EMPLOYEUR, c.DATE_DEBUT,
                   c.DATE_FIN, c.FONCTION_OCCUPEE, c.SUJET, c.ORGANISME, c.TYPE_REMUNERATION,
                   c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS, c.MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS,
                   c.TS_FILIALE, c.TS_GROUPE, c.URL_ANNUAIRE_RPPS, c.URL_DECLARATION_NOM_PRENOM,
                   c.URL_DECLARATION_RPPS]

    return preprocessed_dpi.reset_index(drop=True)[output_cols].sort_values(by=[c.NUMERO_DPI, c.RUBRIQUE])


def extract_common_columns(df: pd.DataFrame) -> pd.DataFrame:
    """ Extract common columns from raw dpi details
    """
    logging.info("- Extraction de nouvelles colonnes à partir du fichier décrivant leur signification.")
    df_signification = pd.read_csv(COLUMNS_SIGNIFICATION_PATH, index_col='rubrique_lien')
    for i in range(1, 12):
        detail_lien_i = 'detail_lien_{}'.format(i)
        for rubrique_lien, new_column in df_signification[detail_lien_i].items():
            if new_column == 'VIDE' or ' ' in new_column or '?' in new_column:
                continue
            mask = df.rubrique_lien == rubrique_lien
            df.loc[mask, new_column] = df.loc[mask, detail_lien_i]
        df = df.drop(columns=[detail_lien_i])
    return df


def normalize_columns(df: pd.DataFrame) -> pd.DataFrame:
    columns_to_normalize = [c.PRENOM_DECLARANT, c.NOM_DECLARANT, c.ORGANISME]
    logging.info("- Normalisation des colonnes '{}'".format(', '.join(columns_to_normalize)))
    for column in columns_to_normalize:
        df[column] = df[column].map(normalize_string)
    return df


def add_column_nom_prenom(df: pd.DataFrame) -> pd.DataFrame:
    """ Add a new column nom_prenom
    """
    df[c.NOM_PRENOM_DECLARANT] = df[c.NOM_DECLARANT].str.upper() + ' ' + \
                                 df[c.PRENOM_DECLARANT].map(lambda s: ' '.join([w.capitalize() for w in s.split()]))
    return df


def add_column_rpps(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add RPPS column from annuaire. At the moment, only add unique fullnames matching
    """
    logging.info("- Ajout d'une colonne identifiant RPPS à partir de l'annuaire des professionnels de santé (pas "
                 "d'homonyme, RPPS seulement)")
    df_annuaire = pd.read_csv(ANNUAIRE_PRO_CLEANED, sep=";")
    df_annuaire = df_annuaire[df_annuaire['type_identifiant'] == 'RPPS']
    return pd.merge(df, df_annuaire, on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], how='left')


def add_date_debut_fin(df: pd.DataFrame) -> pd.DataFrame:
    """ Add 2 new column date_debut & date_fin
    """

    df[c.DATE_DEBUT] = pd.NaT
    df[c.DATE_FIN] = pd.NaT
    mask = df.date_debut_fin.notna()
    df_split_date = (df[mask]
                     .date_debut_fin
                     .fillna('')
                     .str.replace("0201", "2001")  # unique manual error
                     .str.split()
                     )

    df.loc[mask, c.DATE_DEBUT] = pd.to_datetime(df_split_date.str[0])
    df.loc[mask, c.DATE_FIN] = pd.to_datetime(df_split_date.str[2].replace("aujourd'hui", pd.NaT))
    df = df.drop(columns=["date_debut_fin"])
    return df


def filter_out_activity_only_dpi(df: pd.DataFrame) -> pd.DataFrame:
    logging.info("- Groupby des rubriques traitées pour chaque numéro dpi.")
    gb_num_dpi = df.groupby([c.NUMERO_DPI], as_index=False).agg({c.RUBRIQUE: set})

    activity_only_dpis = pd.unique(gb_num_dpi[gb_num_dpi[c.RUBRIQUE] == {"§ 1. Activité Principale"}][c.NUMERO_DPI])

    logging.info("- Exclusion des dpi qui ne renseignent que l’activité principale")
    dpi_not_only_activity = df[~df[c.NUMERO_DPI].isin(activity_only_dpis)]

    return dpi_not_only_activity


def drop_dpi_duplicates(dpi_not_only_activity: pd.DataFrame) -> pd.DataFrame:
    logging.info("- Drop duplicates.")
    dpi_dd = dpi_not_only_activity.drop(
        [c.INSTITUTION, c.INSTANCE, c.DATE_DEBUT_INSTANCE, c.DATE_FIN_INSTANCE, c.FONCTION_DECLARANT, c.QUALITE,
         c.STATUT_DECLARATION, c.DATE_DEBUT_MANDAT, c.DATE_FIN_MANDAT], axis=1).drop_duplicates()

    return dpi_dd


def keep_last_dpi(dpi_dd: pd.DataFrame) -> pd.DataFrame:
    logging.info("- Keep last DPI.")
    extract_dd = dpi_dd[[c.DATE_SOUMISSION_DPI, c.NUMERO_DPI, c.NOM_PRENOM_DECLARANT]].copy()
    map_date = {d: pd.to_datetime(d, dayfirst=True) for d in set(extract_dd[c.DATE_SOUMISSION_DPI].unique())}
    extract_dd[c.DATE_SOUMISSION_DPI] = extract_dd[c.DATE_SOUMISSION_DPI].map(map_date)
    last_numero_dpi_du_nom_prenom = set(
        extract_dd[[c.NUMERO_DPI, c.NOM_PRENOM_DECLARANT, c.DATE_SOUMISSION_DPI]]
            .sort_values(by=[c.DATE_SOUMISSION_DPI, c.NUMERO_DPI], ascending=[False, False])
            .drop_duplicates(c.NOM_PRENOM_DECLARANT, keep="first")[c.NUMERO_DPI])
    mask_last_numero_dpi = extract_dd[c.NUMERO_DPI].isin(last_numero_dpi_du_nom_prenom)
    last_dpi_df = dpi_dd.loc[mask_last_numero_dpi]

    return last_dpi_df


def get_dpi_rubr_2_rem(dpi_dd: pd.DataFrame) -> pd.DataFrame:
    logging.info("- Filtre sur les rubriques 1 et 2 et exclusion des dpi sans rémunération.")
    dpi_rubr_2 = dpi_dd[dpi_dd[c.RUBRIQUE].apply(lambda x: x[:3]).isin(["§ 1", "§ 2"])]
    dpi_rubr_2_rem = dpi_rubr_2[dpi_rubr_2[c.TYPE_REMUNERATION] != "Aucune rémunération"]

    rubr_2_cols = [c.DATE_SOUMISSION_DPI, c.NUMERO_DPI, c.RUBRIQUE, c.NOM_PRENOM_DECLARANT, c.IDENTIFIANT_PROFESSIONNEL,
                   c.TS_TYPE_IDENTIFIANT, c.TYPE_ACTIVITE, c.IS_HEALTHCARE, c.ADRESSE_EMPLOYEUR, c.DATE_DEBUT,
                   c.DATE_FIN, c.FONCTION_OCCUPEE, c.SUJET, c.ORGANISME, c.TYPE_REMUNERATION,
                   c.MONTANT_REMUNERATION_DECLARANT, c.MONTANT_REMUNERATION_ORGANISME]

    return dpi_rubr_2_rem[rubr_2_cols]


def add_column_TS_groupe(df_dpi: pd.DataFrame) -> pd.DataFrame:
    """ Jointure avec le fichier gsheet pour ajouter la colonne groupe
    """
    df_organisme_to_TS_groupe = pd.read_csv(LIEN_ORGANISME_GROUPE_TS, sep=";")
    return df_dpi.merge(df_organisme_to_TS_groupe, how="left", on=c.ORGANISME, validate="many_to_one")
