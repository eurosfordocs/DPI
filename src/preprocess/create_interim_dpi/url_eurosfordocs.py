import pandas as pd

from src.constants import columns as c

BASE_URL = "https://www.eurosfordocs.fr/metabase/dashboard/2?"
BASE_ANNUAIRE_URL = "https://www.eurosfordocs.fr/metabase/dashboard/4?"
NOM_PRENOM_PARAM = "nom_pr_nom_du_professionel="
RPPS_PARAM = "identifiant_du_professionel="
GROUPE_PARAM = "entreprise="


def add_eurosfordocs_url(df: pd.DataFrame):
    mask_rubrique_1 = df.rubrique_lien.str.startswith("§ 1")
    mask_groupe_notna = df.TS_groupe.notna()
    mask_professionnel_notna = df[c.IDENTIFIANT_PROFESSIONNEL].notna()

    df[c.URL_DECLARATION_NOM_PRENOM] = ""
    df.loc[mask_rubrique_1, c.URL_DECLARATION_NOM_PRENOM] = BASE_URL + NOM_PRENOM_PARAM + df[
        c.NOM_PRENOM_DECLARANT]
    df.loc[mask_groupe_notna, c.URL_DECLARATION_NOM_PRENOM] = (BASE_URL +
                                                               NOM_PRENOM_PARAM + df[c.NOM_PRENOM_DECLARANT] + '&' +
                                                               GROUPE_PARAM + df[c.TS_GROUPE]
                                                               )

    df[c.URL_DECLARATION_RPPS] = ""
    df.loc[mask_rubrique_1 & mask_professionnel_notna, c.URL_DECLARATION_RPPS] = (
            BASE_URL + RPPS_PARAM + df[c.IDENTIFIANT_PROFESSIONNEL]
    )
    df.loc[mask_groupe_notna & mask_professionnel_notna, c.URL_DECLARATION_RPPS] = (
            BASE_URL +
            RPPS_PARAM + df[c.IDENTIFIANT_PROFESSIONNEL] + '&' +
            GROUPE_PARAM + df[c.TS_GROUPE]
    )

    df[c.URL_ANNUAIRE_RPPS] = ""
    df.loc[mask_rubrique_1 & mask_professionnel_notna, c.URL_ANNUAIRE_RPPS] = (
            BASE_ANNUAIRE_URL + "identifiant=" + df[c.IDENTIFIANT_PROFESSIONNEL]
    )

    for url in [c.URL_DECLARATION_NOM_PRENOM, c.URL_DECLARATION_RPPS, c.URL_ANNUAIRE_RPPS]:
        df[url] = df[url].str.replace(" ", "%20")
    return df
