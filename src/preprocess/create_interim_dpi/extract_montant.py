import logging

import pandas as pd
from datetime import datetime


from src.constants import columns as c
from src.utils import normalize_string


def extract_montant(df: pd.DataFrame) -> pd.DataFrame:
    """ Ajout de 6 colonnes pour chaque colonne montant : la valeur, la devise et la fréquence

    - 4000 dollars canadiens mensuel => 4000, dollars canadiens, mensuel
    """

    logging.info("- Extraction information montant")

    montant_declarant_cols = [c.MONTANT_REMUNERATION_DECLARANT, c.MONTANT_REMUNERATION_DECLARANT_VALEUR,
                              c.MONTANT_REMUNERATION_DECLARANT_DEVISE, c.MONTANT_REMUNERATION_DECLARANT_FREQUENCE,
                              c.MONTANT_REMUNERATION_DECLARANT_TOTAL, c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS]
    montant_organisme_cols = [c.MONTANT_REMUNERATION_ORGANISME, c.MONTANT_REMUNERATION_ORGANISME_VALEUR,
                              c.MONTANT_REMUNERATION_ORGANISME_DEVISE, c.MONTANT_REMUNERATION_ORGANISME_FREQUENCE,
                              c.MONTANT_REMUNERATION_ORGANISME_TOTAL, c.MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS]

    for (montant_remuneration, montant_remuneration_valeur, montant_remuneration_devise, montant_remuneration_frequence,
         montant_remuneration_total, montant_remuneration_total_euros) in [montant_declarant_cols, montant_organisme_cols]:

        df["parse_montant"] = df[montant_remuneration].apply(parse_montant)

        for col in [montant_remuneration_valeur, montant_remuneration_devise, montant_remuneration_frequence]:
            df[col] = None
        df[montant_remuneration_total] = 0
        df[montant_remuneration_total_euros] = 0

        df[[montant_remuneration_valeur, montant_remuneration_devise, montant_remuneration_frequence]] = df[
            "parse_montant"].tolist()

        df.drop("parse_montant", axis=1, inplace=True)

        df = compute_full_amounts_in_euros(
            df, montant_remuneration_valeur, montant_remuneration_devise, montant_remuneration_frequence,
            montant_remuneration_total, montant_remuneration_total_euros)

    return df


def parse_montant(montant):
    montant_str = str(montant)
    if montant_str in ["Montant non connu", "nan"] or montant_str is None:
        return [None, None, None]
    else:
        if montant_str.split(" ")[-1] in ["Total", "Mensuel", "Annuel"]:
            try:
                montant = int(float(montant_str.split(" ")[0]))
                # Currencies with multiple words like ’dollars canadiens’ are correctly parsed.
                currency = " ".join(montant_str.split(" ")[1:-1])
                frequency = montant_str.split(" ")[-1]
                return [montant, currency, frequency]
            except:
                return [None, None, None]
        return [None, None, None]


def compute_full_amounts_in_euros(df, montant_remuneration_valeur, montant_remuneration_devise,
                                  montant_remuneration_frequence, montant_remuneration_total,
                                  montant_remuneration_total_euros):

    df["date_fin_no_na"] = df[c.DATE_FIN].fillna(
        df[c.DATE_SOUMISSION_DPI].apply(lambda x: datetime.strptime(x, "%d/%m/%Y")))

    for index, row in df.iterrows():
        if row[montant_remuneration_frequence] == "Total":
            df.at[index, montant_remuneration_total] = float(row[montant_remuneration_valeur])
        elif row[montant_remuneration_frequence] == "Annuel":
            nb_years = (row["date_fin_no_na"] - row[c.DATE_DEBUT]).days / 365
            df.at[index, montant_remuneration_total] = float(row[montant_remuneration_valeur]) * nb_years
        elif row[montant_remuneration_frequence] == "Mensuel":
            try:
                nb_months = (row["date_fin_no_na"] - row[c.DATE_DEBUT]).days / (365/12)
            except:
                a = 1
            df.at[index, montant_remuneration_total] = float(row[montant_remuneration_valeur]) * nb_months

        # Conversion en euros
        currency = normalize_string(str(row[montant_remuneration_devise]))
        if currency != "none":
            try:
                df.at[index, montant_remuneration_total_euros] = (
                        df.at[index, montant_remuneration_total] * c.CONVERSION_RATES[currency])
            except:
                raise ValueError("La devise {} n’est pas dans le dictionnaire de conversion.".format(currency))

    df.drop("date_fin_no_na", axis=1, inplace=True)

    return df
