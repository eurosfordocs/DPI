import logging
import pandas as pd

from src.constants import columns as c
from src.constants.files import PROFESSIONNELS_SANTE_PATH, ANNUAIRE_PRO_CLEANED
from src.utils import normalize_string


def preprocess_annuaire_pro(drop_homonyms=True) -> None:
    """
    Read and clean annuaire professionnels, keeping name, last_name and rpps-only id columns
    """
    logging.info("Preprocessing annuaire pro.")
    df_annuaire = pd.read_csv(PROFESSIONNELS_SANTE_PATH, sep=';', compression='gzip', low_memory=False,
                              usecols=['nom', 'prénom', 'type_identifiant', c.IDENTIFIANT_PROFESSIONNEL],
                              )

    df_annuaire = df_annuaire.dropna(subset=['nom', 'prénom'])
    df_annuaire[c.NOM_DECLARANT] = df_annuaire['nom'].map(normalize_string)
    df_annuaire[c.PRENOM_DECLARANT] = df_annuaire['prénom'].map(normalize_string)

    if drop_homonyms:
        df_annuaire = df_annuaire.drop_duplicates(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], keep=False)

    df_annuaire.drop(columns=['nom', 'prénom']).to_csv(ANNUAIRE_PRO_CLEANED, sep=";", index=False)
