import logging
from os.path import join

import pandas as pd

from src.constants import columns as c
from src.constants.files import (RAW_DATA_DIR, INTERIM_DATA_DIR, ANNUAIRE_PRO_CLEANED, TS_AVANTAGE, TS_CONVENTION,
                                 TS_REMUNERATION, TS_CONCATENATED)
from src.utils import normalize_string


def create_interim_ts(chunksize: int):
    """
    Filter and clean TS data for later matching with DPIs.

    :param chunksize: Size of the computation chunks.
    :return: None
    """
    df_list = []
    for ts_file_name in [TS_AVANTAGE, TS_CONVENTION, TS_REMUNERATION]:
        logging.info("- Création d'un fichier TS intermédiaire, à partir du fichier '{}'".format(ts_file_name))
        for count, chunk in enumerate(
                pd.read_csv(join(RAW_DATA_DIR, ts_file_name), sep=';', compression='gzip', chunksize=chunksize,
                            low_memory=False)):
            logging.info("  - Preprocessing du chunk {}".format(count + 1))
            chunk = (chunk
                     .pipe(filter_non_relevant)
                     .pipe(complete_missing_rpps_ts)
                     .pipe(keep_relevant_ts_columns, origin=ts_file_name)
                     )
            df_list.append(chunk)
    interim_ts_file_path = join(INTERIM_DATA_DIR, TS_CONCATENATED)
    logging.info("- Écriture du dataframe de TS intermédiaire à l'emplacement '{}'".format(interim_ts_file_path))
    df_interim = pd.concat(df_list, sort=False).drop_duplicates()
    df_interim.to_csv(interim_ts_file_path, sep=';', index=False, compression='gzip')


def filter_non_relevant(df: pd.DataFrame) -> pd.DataFrame:
    non_dpi_categories = ['Restauration', 'Hospitalité', 'Transport', "Hébergement"]
    mask_non_dpi_declarations = df[c.TS_CATEGORIE_PRECISE].isin(non_dpi_categories)
    return df[~mask_non_dpi_declarations]


def complete_missing_rpps_ts(df: pd.DataFrame) -> pd.DataFrame:
    """
    Complete RPPS column from annuaire. At the moment, only add unique fullnames matching
    """
    logging.info("  - Complétion de la colonne identifiant RPPS à partir de l'annuaire des professionnels de santé "
                 "(pas d'homonyme)")
    # Preprocessing annuaire
    df_annuaire = pd.read_csv(ANNUAIRE_PRO_CLEANED, sep=";")
    # Preprocessing TS file to keep only row to complete
    subset_df = df.dropna(subset=[c.TS_NOM_DECLARANT, c.TS_PRENOM_DECLARANT]).copy()
    mask = subset_df[c.TS_IDENTIFIANT].isna()
    subset_df = subset_df[mask]
    # Normalisation of names and surnames
    subset_df.loc[:, c.NOM_DECLARANT] = subset_df[c.TS_NOM_DECLARANT].map(normalize_string)
    subset_df.loc[:, c.PRENOM_DECLARANT] = subset_df[c.TS_PRENOM_DECLARANT].map(normalize_string)
    # Definition of mask of interesting rows
    mask_missing_rpps = df[c.TS_PRENOM_DECLARANT].notnull() & df[c.TS_NOM_DECLARANT].notnull() & \
                        df[c.TS_IDENTIFIANT].isna()
    # Merge and completion
    index_copy = subset_df.index
    subset_df = subset_df.merge(df_annuaire, on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], how='left')
    subset_df.index = index_copy
    # Completion
    df.loc[mask_missing_rpps, c.TS_IDENTIFIANT] = subset_df[c.TS_IDENTIFIANT+'_y']
    df.loc[mask_missing_rpps, c.TS_TYPE_IDENTIFIANT] = subset_df[c.TS_TYPE_IDENTIFIANT+'_y']

    return df


def keep_relevant_ts_columns(df: pd.DataFrame, origin: str) -> pd.DataFrame:
    logging.info("  - Suppression des colonnes inutiles")
    df['origine'] = origin
    columns_to_keep = [c.TS_ORGANISME, c.TS_FILIALE_DECLARANTE, c.TS_IDENTIFIANT, c.TS_TYPE_IDENTIFIANT, c.TS_ORIGIN,
                       c.TS_MONTANT]
    return df[columns_to_keep]