import logging
import os
import re
import shlex
import subprocess

import pandas as pd

import requests
from unidecode import unidecode

import  src.constants.files as f


def download_file_from_url(url: str, file_path: str, overwrite: bool = False) -> None:
    if not overwrite and os.path.exists(file_path):
        logging.info("- File '{}' already exists, we do not overwrite it.".format(file_path))
        return

    logging.info("- Download file at url {} to '{}'".format(url, file_path))
    r = requests.get(url, stream=True)
    with open(file_path, 'wb') as f:
        for chunk in r.iter_content(10 ** 5):
            f.write(chunk)


def normalize_string(s: str) -> str:
    if not isinstance(s, str):
        return s
    s = s.lower()
    s = unidecode(s)  # strip accents
    s = re.sub(r'[^a-zA-Z0-9]', ' ', s)
    s = ' '.join(s.split())
    return s


def get_last_commit_id_str():
    last_commit_sha = exec_terminal("git rev-parse --verify HEAD").strip()
    logging.debug("Le hash SHA du dernier commit est '{}'".format(last_commit_sha))
    last_commit_message = exec_terminal("git log -1 --pretty=%B").strip()
    last_commit_message = '-'.join(last_commit_message.split())
    last_commit_id_str = last_commit_sha[:10] + '_' + last_commit_message[:50]

    last_commit_id_str = unidecode(last_commit_id_str)
    regex = re.compile(r'[^0-9a-zA-Z\_\-]')
    last_commit_id_str = regex.sub('', last_commit_id_str)

    logging.info("Le dernier commit sera identifié à partir du message '{}'".format(last_commit_id_str))
    return last_commit_id_str


def exec_terminal(command: str) -> str:
    logging.debug("Execute: {}".format(command))
    bash_command_list = shlex.split(command)
    return subprocess.check_output(bash_command_list).decode()


def download_all_files() -> None:
    # Déplacé dans utils pour éviter la dépendance circulaire avec src.constants.file
    logging.info("Download all files from external resources.")
    download_file_from_url(f.COLUMNS_SIGNIFICATION_URL, f.COLUMNS_SIGNIFICATION_PATH, overwrite=True)
    download_file_from_url(f.PROFESSIONNELS_SANTE_URL, f.PROFESSIONNELS_SANTE_PATH)
    download_file_from_url(f.TS_AVANTAGE_URL, os.path.join(f.RAW_DATA_DIR, f.TS_AVANTAGE))
    download_file_from_url(f.TS_CONVENTION_URL, os.path.join(f.RAW_DATA_DIR, f.TS_CONVENTION))
    download_file_from_url(f.TS_REMUNERATION_URL, os.path.join(f.RAW_DATA_DIR, f.TS_REMUNERATION))
    download_file_from_url(f.TS_ENTREPRISE_URL, os.path.join(f.RAW_DATA_DIR, f.TS_ENTREPRISE))
    download_file_from_url(f.COMPANY_GROUP_URL, f.COMPANY_GROUP_PATH, overwrite=True)
    download_file_from_url(f.DPI_TO_TS_ORGANISME_URL, f.DPI_TO_TS_ORGANISME_PATH, overwrite=True)


def sign_cols_dict():
    """
    Return dictionary {section (rubrique) name: columns corresponding to the section}.
    """
    sign_cols = pd.read_csv(f.COLUMNS_SIGNIFICATION_PATH, sep=",").drop(
        ["Page manuel", "Nb lignes"], axis=1)

    sign_cols_dict = {}

    for index, row in sign_cols.iterrows():
        detail_liens_list = [row["detail_lien_{}".format(str(x))] for x in range(1, 11)
                             if row["detail_lien_{}".format(str(x))] not in ["VIDE", "date_debut_fin"]]
        for montant_col in ["montant_remuneration_organisme", "montant_remuneration_declarant"]:
            if montant_col in detail_liens_list:
                detail_liens_list += [montant_col + "_" + suf for suf in ["valeur", "devise", "frequence"]]
        if "organisme_financeur_montant_pourcentage" in detail_liens_list:
            detail_liens_list += ["organisme_financeur", "montant_activites_financees",
                                  "pourcentage_activites_financees"]

        detail_liens_list += ["date_debut", "date_fin", "numero_dpi", "nom_prenom_declarant"]
        sign_cols_dict[row["rubrique_lien"]] = detail_liens_list

    return sign_cols_dict
