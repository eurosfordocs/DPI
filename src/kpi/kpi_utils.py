import pandas as pd
from typing import Tuple

from src.constants import columns as c
from src.constants.columns import IS_HEALTHCARE


def keep_unique_dpi(df_dpi_categorized: pd.DataFrame) -> pd.DataFrame:
    """
    Keeps unique dpi number
    :param df_dpi_categorized:
    :return: DataFrame
    """
    return df_dpi_categorized.drop_duplicates(subset=[c.NUMERO_DPI])


def concat_healthcare_dpi(df_dpi_categorized: pd.DataFrame) -> pd.DataFrame:
    """
    Keeps unique dpi number, categorized as healthcare or with rpps/adeli id
    :param df_dpi_categorized:
    :return: DataFrame
    """
    df_healthcare_only = df_dpi_categorized[df_dpi_categorized[IS_HEALTHCARE] == 'yes']
    df_rpps_adeli_not_healthcare = df_dpi_categorized[
        (df_dpi_categorized[IS_HEALTHCARE] != 'yes') & (~df_dpi_categorized[c.TS_TYPE_IDENTIFIANT].isnull())]
    return pd.concat([df_healthcare_only, df_rpps_adeli_not_healthcare], sort=False)


def compute_ratio(df_1: pd.DataFrame, df_2: pd.DataFrame) -> float:
    """
    Compute ratio of lengths of input dataframes.
    :return: ratio
    """
    return round(100 * (len(df_1)) / len(df_2), 1)


def filter_dpi_on_people_match(df_dpi: pd.DataFrame) -> float:
    """
    Filter dpi dataframe on rows with people id matched among healthcare dpi
    :return: filtered dpi
    """
    return df_dpi[~df_dpi[c.TS_TYPE_IDENTIFIANT].isnull()]


def filter_dpi_on_company_match(df_dpi: pd.DataFrame) -> float:
    """
    Filter dpi dataframe on rows with companies matched among healthcare dpi.
    :return: filtered dpi
    """
    return df_dpi[~df_dpi[c.TS_GROUPE].isnull()]


def filter_dpi_on_people_company_match(df_dpi: pd.DataFrame) -> float:
    """
    Filter dpi dataframe on rows with companies matched among healthcare dpi.
    :return: filtered dpi
    """
    return df_dpi[
        ~df_dpi[c.TS_GROUPE].isnull()
        &~df_dpi[c.TS_IDENTIFIANT].isnull()]


def compute_nb_homonymes(dpi_full: pd.DataFrame, df_annuaire: pd.DataFrame) -> Tuple[str, str]:
    # Keep homonyms
    df_annuaire_homonyms = df_annuaire[
        df_annuaire.duplicated(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT], keep=False)].drop_duplicates(
        subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT])
    df_dpi_people = dpi_full.copy()
    df_dpi_people = df_dpi_people.drop_duplicates(subset=[c.NOM_DECLARANT, c.PRENOM_DECLARANT])
    df_dpi_people_match = pd.merge(
        df_dpi_people,
        df_annuaire_homonyms,
        on=[c.NOM_DECLARANT, c.PRENOM_DECLARANT],
        how='inner'
    )
    nb_homonyms = str(len(df_dpi_people_match))
    homonyms_ratio = str(round(100 * len(df_dpi_people_match) / len(df_dpi_people), 1)) + "%"
    return nb_homonyms, homonyms_ratio

