import logging
from datetime import datetime
from os.path import join

import pandas as pd

import src.constants.columns as c
from src.constants.files import KPI_PATH, INTERIM_DATA_DIR, DPI_RUBR_2, RESULT_DATA_DIR, DIFF_DPI_TS
from src.kpi.kpi_utils import (compute_ratio, concat_healthcare_dpi, filter_dpi_on_people_match,
                               filter_dpi_on_company_match, filter_dpi_on_people_company_match)
from src.utils import get_last_commit_id_str


def update_kpi() -> None:
    logging.info("Calcul des KPI du projet.")

    # Computing the new kpis
    dpi_rubr_2_rem_file_path = join(INTERIM_DATA_DIR, DPI_RUBR_2)
    dpi_rubr_2_rem = pd.read_csv(dpi_rubr_2_rem_file_path, sep=";", low_memory=False)

    kpi_row = compute_kpi(dpi_rubr_2_rem)
    kpi_columns = list(kpi_row.keys())

    kpi_row["last_commit"] = get_last_commit_id_str()
    kpi_row['timestamp'] = datetime.now()

    try:
        df = pd.read_csv(KPI_PATH, low_memory=False)

        new_cols = [col for col in kpi_columns if col not in df.columns]
        for col in new_cols:
            df[col] = None
    except FileNotFoundError:
        df = None

    new_kpi_df = pd.concat([df, pd.DataFrame.from_records([kpi_row])], sort=False)

    new_kpi_df_dd = new_kpi_df.drop_duplicates(subset=kpi_columns)

    if df is not None and not df.iloc[-1][kpi_columns].equals(pd.DataFrame.from_records([kpi_row])):
        new_kpi_df_dd[['last_commit', 'timestamp'] + kpi_columns].to_csv(KPI_PATH, index=False)
    elif df is not None:
        logging.info("- Pas de différence entre les nouvelles et anciennes KPI.")


def compute_kpi(dpi_rubr_2_rem):

    df_healthcare = concat_healthcare_dpi(dpi_rubr_2_rem)

    sum_amounts_rubr_2_declarant = round(df_healthcare[c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS].sum() / 10 ** 6, 1)
    sum_amounts_rubr_2_organisme = round(df_healthcare[c.MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS].sum() / 10 ** 6, 1)

    dpi_matchables = filter_dpi_on_people_company_match(df_healthcare)

    sum_amounts_matchables_declarant = round(
        dpi_matchables[c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS].sum() / 10 ** 6, 1)
    sum_amounts_matchables_organisme = round(
        dpi_matchables[c.MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS].sum() / 10 ** 6, 1)

    # nb_homonymes, homonymes_ratio = compute_nb_homonymes(dpi_full, df_annuaire)
    nb_dpi_rubr_2_rem = dpi_rubr_2_rem[c.NUMERO_DPI].nunique()

    diff_dpi_ts = pd.read_csv(join(RESULT_DATA_DIR, DIFF_DPI_TS), sep=";")
    sum_missing_amounts_declarant = round(
        diff_dpi_ts[c.MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS].sum() / 10 ** 6, 1)
    sum_missing_amounts_organisme = round(
        diff_dpi_ts[c.MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS].sum() / 10 ** 6, 1)


    return {
        "Nb dpi §2 avec paiement": nb_dpi_rubr_2_rem,
        "Nb lignes dpi §2 avec paiement": len(dpi_rubr_2_rem),
        "% lignes dpi §2 santé": convert_to_str(compute_ratio(df_healthcare, dpi_rubr_2_rem)),
        "Somme des montants déclarants §2 santé (M€)": sum_amounts_rubr_2_declarant,
        "Somme des montants organismes §2 santé (M€)": sum_amounts_rubr_2_organisme,
        "% lignes dpi §2 santé matchées personne": convert_to_str(
            compute_ratio(filter_dpi_on_people_match(df_healthcare), df_healthcare)),
        "% lignes dpi §2 santé matchées entreprise": convert_to_str(
            compute_ratio(filter_dpi_on_company_match(df_healthcare), df_healthcare)),
        "% lignes dpi §2 santé matchées personne entreprise": convert_to_str(
            compute_ratio(dpi_matchables, df_healthcare)),
        "Nb lignes dpi §2 santé matchées personne entreprise": len(dpi_matchables),
        "Somme des montants déclarants §2 santé matchables (M€)": sum_amounts_matchables_declarant,
        "Somme des montants organismes §2 santé matchables (M€)": sum_amounts_matchables_organisme,
        "Nb lignes dpi §2 santé matchables manquantes dans TS": len(diff_dpi_ts),
        "Somme des montants déclarants manquant (M€)": sum_missing_amounts_declarant,
        "Somme des montants organismes manquant (M€)": sum_missing_amounts_organisme

    }


def convert_to_str(a: float) -> str:
    return str(a) + "%"
