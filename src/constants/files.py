from os.path import join
import os
import logging
from datetime import datetime


def create_folder(path):
    if not os.path.isdir(path):
        os.mkdir(path)
    return path


TEST_SUFFIX = os.getenv("TEST_SUFFIX", "")

ROOT_PATH = join(os.path.dirname(os.path.abspath(__file__)), "..", "..")
DATA_DIR = create_folder(join(ROOT_PATH, f"data{TEST_SUFFIX}"))
RAW_DATA_DIR = create_folder(join(DATA_DIR, 'raw'))
INTERIM_DATA_DIR = create_folder(join(DATA_DIR, 'interim'))
RESULT_DATA_DIR = create_folder(join(DATA_DIR, 'result'))

LOG_PATH = create_folder(os.path.join(DATA_DIR, "logs"))

today_str = datetime.now().strftime("%Y-%m-%d")
LOG_FILE = os.path.join(LOG_PATH, today_str + ".log")
logging_format = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(filename=LOG_FILE, level=logging.INFO, format=logging_format)

# set up logging to console
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(logging.Formatter(logging_format))
logging.getLogger('').addHandler(console)

DPI_FILE = "20190903001500_export_declarations_publiees.csv"
DPI_RUBR_2 = DPI_FILE.split(".")[0] + "_rubr_2_rem." + ".".join(DPI_FILE.split(".")[1:])

DIFF_DPI_TS = "diff_dpi_ts.csv"

COLUMNS_SIGNIFICATION_URL = \
    "https://docs.google.com/spreadsheets/d/1zkMSWEDfKc-eG4LPdUREr_yzJc6urJgtEiv7LLPqThE/export?format=csv"
COLUMNS_SIGNIFICATION_PATH = join(RAW_DATA_DIR, "signification_colonnes.csv")


PROFESSIONNELS_SANTE_URL = "https://eurosfordocs.fr/download/dump/professionnel_sante.csv.gz"
PROFESSIONNELS_SANTE_PATH = join(RAW_DATA_DIR, "professionnel_sante.csv.gz")
ANNUAIRE_PRO_CLEANED = join(INTERIM_DATA_DIR, "annuaire_pro_cleaned.csv")

TS_AVANTAGE_URL = "https://eurosfordocs.fr/download/dump/declaration_avantage.csv.gz"
TS_CONVENTION_URL = "https://eurosfordocs.fr/download/dump/declaration_convention.csv.gz"
TS_REMUNERATION_URL = "https://eurosfordocs.fr/download/dump/declaration_remuneration.csv.gz"
TS_ENTREPRISE_URL = "https://eurosfordocs.fr/download/dump/entreprise.csv.gz"
TS_AVANTAGE = "declaration_avantage.csv.gz"
TS_CONVENTION = "declaration_convention.csv.gz"
TS_REMUNERATION = "declaration_remuneration.csv.gz"
TS_ENTREPRISE = "entreprise.csv.gz"
TS_CONCATENATED = "declarations_concatenees.csv.gz"

COMPANY_GROUP_URL = "https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/export?format=csv"
COMPANY_GROUP_PATH = join(RAW_DATA_DIR, "association_entreprise_groupe.csv")

DPI_TO_TS_ORGANISME_URL = "https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/export?format=csv&id=1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E&gid=1493365784"
DPI_TO_TS_ORGANISME_PATH = join(RAW_DATA_DIR, "dpi_to_ts_organisme.csv")

LIEN_ORGANISME_GROUPE_TS = join(INTERIM_DATA_DIR, "lien_organisme_groupe_TS.csv")

KPI_PATH = join(RESULT_DATA_DIR, "kpi.csv")
