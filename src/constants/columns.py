RAW_HEADER = [
    'institution',
    'instance',
    'date_debut_instance',
    'date_fin_instance',
    'nom_declarant',
    'prenom_declarant',
    'fonction_declarant',
    'qualite',
    'statut_declaration',
    'date_debut_mandat',
    'date_fin_mandat',
    'numero_dpi',
    'date_soumission_dpi',
    'rubrique_lien',
    'detail_lien_1',
    'detail_lien_2',
    'detail_lien_3',
    'detail_lien_4',
    'detail_lien_5',
    'detail_lien_6',
    'detail_lien_7',
    'detail_lien_8',
    'detail_lien_9',
    'detail_lien_10',
    'detail_lien_11'
]

CONVERSION_RATES = {"euros": 1, "dollars": 0.9, "chf": 0.9, "dollars canadiens": 0.7, "dollars americain": 0.9,
                   "livre sterling": 1.2, "dollars americains": 0.9}

NUMERO_DPI = "numero_dpi"
RUBRIQUE = "rubrique_lien"

INSTITUTION = 'institution'
INSTANCE = 'instance'
DATE_DEBUT_INSTANCE = 'date_debut_instance'
DATE_FIN_INSTANCE = 'date_fin_instance'
FONCTION_DECLARANT = 'fonction_declarant'
QUALITE = 'qualite'
STATUT_DECLARATION = 'statut_declaration'
DATE_DEBUT_MANDAT = 'date_debut_mandat'
DATE_FIN_MANDAT = 'date_fin_mandat'
TYPE_REMUNERATION = "type_remuneration"

PROFESSION_NORM = 'profession_norm'
PROFESSION = 'profession'

PRENOM_DECLARANT = 'prenom_declarant'
NOM_DECLARANT = 'nom_declarant'
NOM_PRENOM_DECLARANT = 'nom_prenom_declarant'
FONCTION_OCCUPEE = 'fonction_occupee'
FONCTION_OCCUPEE_NORM = 'fonction_occupee_norm'
ADRESSE_EMPLOYEUR = "adresse_employeur"
TYPE_ACTIVITE = "type_activite"
IDENTIFIANT_PROFESSIONNEL = 'identifiant'
DATE_FIN = 'date_fin'
DATE_DEBUT = 'date_debut'
DATE_SOUMISSION_DPI = 'date_soumission_dpi'
ORGANISME_MONTANT_POURCENTAGE = 'organisme_financeur_montant_pourcentage'
ORGANISME = 'organisme_financeur'
SUJET = "sujet"
MONTANT_ACTIVITES_FINANCEES = 'montant_activites_financees'
POURCENTAGE_ACTIVITES_FINANCEES = 'pourcentage_activites_financees'
MONTANT_REMUNERATION_DECLARANT = 'montant_remuneration_declarant'
MONTANT_REMUNERATION_ORGANISME = 'montant_remuneration_organisme'
MONTANT_REMUNERATION_DECLARANT_VALEUR = 'montant_remuneration_declarant_valeur'
MONTANT_REMUNERATION_DECLARANT_DEVISE = 'montant_remuneration_declarant_devise'
MONTANT_REMUNERATION_DECLARANT_FREQUENCE = 'montant_remuneration_declarant_frequence'
MONTANT_REMUNERATION_DECLARANT_TOTAL = "montant_remuneration_declarant_total"
MONTANT_REMUNERATION_DECLARANT_TOTAL_EUROS = "montant_remuneration_declarant_total_euros"

MONTANT_REMUNERATION_ORGANISME_VALEUR = 'montant_remuneration_organisme_valeur'
MONTANT_REMUNERATION_ORGANISME_DEVISE = 'montant_remuneration_organisme_devise'
MONTANT_REMUNERATION_ORGANISME_FREQUENCE = 'montant_remuneration_organisme_frequence'
MONTANT_REMUNERATION_ORGANISME_TOTAL = "montant_remuneration_organisme_total"
MONTANT_REMUNERATION_ORGANISME_TOTAL_EUROS = "montant_remuneration_organisme_total_euros"
IS_HEALTHCARE = 'is_healthcare'
URL_DECLARATION_NOM_PRENOM = "url_declaration_nom_prenom"
URL_DECLARATION_RPPS = "url_declaration_rpps"
URL_ANNUAIRE_RPPS = "url_annuaire_rpps"

TS_GROUPE = "TS_groupe"
TS_PRENOM_DECLARANT = 'prénom'
TS_NOM_DECLARANT = 'nom'
TS_IDENTIFIANT = 'identifiant'
TS_TYPE_IDENTIFIANT = 'type_identifiant'
TS_ORGANISME = 'entreprise_émmetrice'
TS_FILIALE_DECLARANTE = 'filiale_déclarante'
TS_ORIGIN = 'origine'
TS_FILIALE = "TS_filiale"
TS_CATEGORIE_PRECISE = "categorie_precise"
TS_MONTANT = "montant_ttc"

# Param
RUBRIQUE_LIEN_1 = '§ 1. Activité Principale'
