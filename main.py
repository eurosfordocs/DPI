#!/usr/bin/env python

import click
import logging

from src.dpi_ts_matching import get_dpi_not_in_ts
from src.kpi.kpi import update_kpi
from src.preprocess.annuaire_pro import preprocess_annuaire_pro
from src.preprocess.lien_organisme_groupe_TS import preprocess_lien_organisme_groupe_ts
from src.preprocess.create_interim_dpi.create_interim_dpi import create_interim_dpi
from src.preprocess.create_interim_ts import create_interim_ts
from src.utils import download_all_files


@click.command("Automatisation du travail sur les DPI.")
@click.option('--no-download', '-nd', is_flag=True, help="Do not download external files.")
@click.option('--no-process', '-np', is_flag=True, help="Do not process data to create interim files.")
@click.option('--no-interim-ts', '-nts', is_flag=True, help="Do not process data to create interim TS files.")
@click.option('--chunk-size', '-cs', type=int, default=10 ** 6, help="Size of the chunk used to process ts files.")
@click.option('--kpi', is_flag=True, help="Compute KPI on project.")
def cli(no_download, no_process, no_interim_ts, chunk_size, kpi):
    logging.info("Launching main.")

    if not no_download:
        download_all_files()

    if not no_process:
        preprocess_annuaire_pro()
        preprocess_lien_organisme_groupe_ts()
        create_interim_dpi()
        if not no_interim_ts:
            create_interim_ts(chunk_size)
        get_dpi_not_in_ts()

    if kpi:
        update_kpi()


if __name__ == '__main__':
    cli()
